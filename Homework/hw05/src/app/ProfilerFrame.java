package app;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Vector;

import javax.swing.*;

public class ProfilerFrame extends JFrame {
    private Profiler p;
    ProfilerFrame(Profiler p){
        super("Profiler Information");
        this.p = p;
        setUpGUI();
    }

    private void setUpGUI(){
        JTabbedPane origin = new JTabbedPane();
        
        HashMap<String, LinkedList<ProfilerTime>> timers = p.getTimers();
        HashMap<String, Integer> counters = p.getCounters();

        {
            Vector<String> columnNames = new Vector<String>();
            columnNames.add("<html><b>ID</b></html>");
            columnNames.add("<html><b>Count</b></html>");

            Vector<Vector<Object>> data = new Vector<Vector<Object>>();

            Iterator it = counters.entrySet().iterator();
            while(it.hasNext()){
                HashMap.Entry pair = (HashMap.Entry)it.next();
                String ID = (String) pair.getKey();
                Integer count = (Integer)pair.getValue();
                Vector<Object> v = new Vector<Object>();
                v.addElement(ID);
                v.addElement(count);
                data.add(v);
            }
            JTable counterPanel= new JTable(data, columnNames);
            origin.addTab("Counters", new JScrollPane(counterPanel));
        }
        
        {
            Vector<String> columnNames = new Vector<String>();
            columnNames.add("<html><b>ID</b></html>");
            columnNames.add("<html><b>Average Duration</b></html>");
            columnNames.add("<html><b>Longest</b></html>");
            columnNames.add("<html><b>Shortest</b></html>");

            Vector<Vector<Object>> data = new Vector<Vector<Object>>();

            Iterator it = timers.entrySet().iterator();

            while(it.hasNext()){
                //initialize values for the data of each timer/ID
                double numEles = 1;
                double sumDuration = 0;
                double longest;
                double shortest;

                //get the required parts of the timer info
                HashMap.Entry pair = (HashMap.Entry)it.next();
                String ID = (String) pair.getKey();
                LinkedList<ProfilerTime> times = (LinkedList<ProfilerTime>) pair.getValue();

                //deal with the first element in the list
                longest = shortest = times.getLast().getDuration().doubleValue();
                sumDuration += longest;

                //loop through all the elements checking for the summary data values
                //ie. shortest duration, longest, and total duration
                for( ProfilerTime t : times){
                    double d = t.getDuration().doubleValue();
                    if(d>longest)
                        longest = d;
                    if(d<shortest)
                        shortest = d;
                    sumDuration += d;
                    numEles += 1.0;
                }

                //add the information from each ID to the table
                Vector<Object> v = new Vector<Object>();
                v.addElement(ID);
                v.addElement(new Double(sumDuration/numEles));
                v.addElement(new Double(longest));
                v.addElement(new Double(shortest));

                //actually add it to the table
                data.add(v);
            }

            JTable summaryPanel= new JTable(data, columnNames);

            origin.addTab("Profiler Summary", new JScrollPane(summaryPanel));
        }

        {
            Vector<String> columnNames = new Vector<String>();
            columnNames.add("<html><b>ID</b></html>");
            columnNames.add("<html><b>Start</b></html>");
            columnNames.add("<html><b>Stop</b></html>");
            columnNames.add("<html><b>Duration</b></html>");
            columnNames.add("<html><b>Start Msg</b></html>");
            columnNames.add("<html><b>End Msg</b></html>");

            Vector<Vector<Object>> data = new Vector<Vector<Object>>();

            Iterator it = timers.entrySet().iterator();

            while(it.hasNext()){
                //get the required parts of the timer info
                HashMap.Entry pair = (HashMap.Entry)it.next();
                String ID = (String) pair.getKey();
                LinkedList<ProfilerTime> times = (LinkedList<ProfilerTime>) pair.getValue();

                for(ProfilerTime t : times){
                    String startTime = t.getStart();
                    String endTime = t.getEnd();
                    Double duration = t.getDuration();
                    String startMsg = t.getStartMessage();
                    String endMsg = t.getEndMessage();
                    //add the information from each ID to the table
                    Vector<Object> v = new Vector<Object>();
                    v.addElement(ID);
                    v.addElement(startTime);
                    v.addElement(endTime);
                    v.addElement(duration);
                    v.addElement(startMsg);
                    v.addElement(endMsg);
    
                    //actually add it to the table
                    data.add(v);
                }
            }

            JTable detailPanel= new JTable(data, columnNames);

            origin.addTab("Profiler Details", new JScrollPane(detailPanel));
        }

        add(origin);

        setLocation(100,100);
        setSize(800,500);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        setVisible(true);
    }
}