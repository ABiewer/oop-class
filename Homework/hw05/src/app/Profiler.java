package app;

import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;

public class Profiler {
    private static Profiler p = null;
    private static boolean enabled;
    private static HashMap<String, LinkedList<ProfilerTime>> timers;
    private static HashMap<String, Integer> counters;

    /**
     * Creates a new Profiler, only called if the static profiler has not been created yet. 
     */
    private Profiler(){
        timers = new HashMap<String, LinkedList<ProfilerTime>>();
        counters = new HashMap<String, Integer>();
        enabled = true;
    }
    /**
     * Returns the Profiler object. If it does not exist, it is created. 
     */
    public static Profiler getInstance(){
        if(p == null){
            p = new Profiler();
        }
        return p;
    }

    /**
     * Starts a timer for the given id and marks down the message given
     * @param id the value for the map key
     * @param message a message for why/what was started
     * @throws ProfilerException when the profiler has an open start that has not been closed
     */
    public void start(String id, String message) throws ProfilerException {
        startHelper(id, message);
    }
    /**
     * Starts a timer for the given id and marks down the message given
     * @param id the value for the map key
     * @throws ProfilerException when the profiler has an open start that has not been closed
     */
    public void start(String id) throws ProfilerException {
        startHelper(id, "");
    }
    private void startHelper(String id, String message) throws ProfilerException {
        if(!enabled)
            return;
        //timers
        if(timers.get(id)==null){
            timers.put(id, new LinkedList<ProfilerTime>());
            timers.get(id).add(new ProfilerTime(message));
        }
        else if(!timers.get(id).getLast().hasEnded()){
            throw new ProfilerException();
        }
        else{
            timers.get(id).add(new ProfilerTime(message));
        }
    }

    public void stop(String id, String message) throws ProfilerException {
        stopHelper(id, message);
    }
    public void stop(String id) throws ProfilerException {
        stopHelper(id, "");
    }
    private void stopHelper(String id, String message) throws ProfilerException {
        if(!enabled)
            return;
        if(timers.get(id) == null){
            throw new ProfilerException();
        }
        else if(timers.get(id).getLast().hasEnded()){
            throw new ProfilerException();
        }
        else{
            timers.get(id).getLast().setEnd(message);
        }
    }

    /**
     * Increments a counter for the given id
     * @param id 
     */
    public void count(String id){
        if(!enabled)
            return;
        if(counters.get(id) == null)
            counters.put(id, 1);
        else
            counters.put(id, counters.get(id) + 1);
    }

    /**
     * Sets whether the Profiler is enabled
     * @param s true/false if the Profiler should be enabled
     */
    public void setEnabled(boolean s){
        enabled = s;
    }
    /**
     * @return whether the Profiler is enabled
     */
    public boolean isEnabled(){
        return enabled;
    }

    /**
     * Generates the JFrame to display the report and relevent information. 
     */
    public void generateReport(){
        //jframe stuff
        ProfilerFrame f = new ProfilerFrame(p);
    }

    private static String debugGenerateReport(){
        StringBuilder sb = new StringBuilder();
        sb.append("DETAILS:\n");
        sb.append("Name     Start       Stop        Duration    StartMsg    EndMsg      \n");
        Iterator it = timers.entrySet().iterator();
        while (it.hasNext()) {
            HashMap.Entry pair = (HashMap.Entry)it.next();
            String name = (String)pair.getKey();
            LinkedList<ProfilerTime> data = (LinkedList<ProfilerTime>)pair.getValue();
            for(int i = 0;i<data.size();i++){
                sb.append(name);
                sb.append("     ");
                sb.append(data.get(i).getStart());
                sb.append("     ");
                sb.append(data.get(i).getEnd());
                sb.append("     ");
                sb.append(data.get(i).getDuration());
                sb.append("     ");
                sb.append(data.get(i).getStartMessage());
                sb.append("     ");
                sb.append(data.get(i).getEndMessage());
                sb.append("     \n");
            }
        }
        return sb.toString();
    }

    public HashMap<String, LinkedList<ProfilerTime>> getTimers(){
        return timers;
    }
    public HashMap<String, Integer> getCounters(){
        return counters;
    }
}