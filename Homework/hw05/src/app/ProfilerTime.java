package app;

import java.util.Date;

public class ProfilerTime {
    private Date start;
    private String sMessage;
    private Date end;
    private String eMessage;
    
    /**
     * Creates a ProfilerTime that holds a value for the start time and the end time and can calculate the difference
     * between the two values. 
     */
    public ProfilerTime(){
        this.start = new Date();
        sMessage = "";
    }

    public ProfilerTime(String sMessage){
        this.start = new Date();
        this.sMessage = sMessage;
    }
    /**
     * Sets the end time for the ProfilerTime
     */
    public void setEnd(){
        this.end = new Date();
        eMessage = "";
    }

    public void setEnd(String eMessage){
        setEnd();
        this.eMessage = eMessage;
    }

    /**
     * @return whether the end time has been set or not
     */
    public boolean hasEnded(){
        return end!=null;
    }
    
    /**
     * Returns the difference in time between when the object was created and the
     * setEnd was called.
     * 
     * @throws ProfilerException when the end time has not been specified
     */
    public long getTime() throws ProfilerException {
        if(end == null)
            throw new ProfilerException();
        return end.getTime()-start.getTime();
    }

    /**
     * @return the message provided at the object's creation
     */
    public String getStartMessage(){
        return sMessage;
    }
    /**
     * @return the message provided at the call to setEnd()
     */
    public String getEndMessage(){
        return eMessage;
    }
    
    /**
     * @return the start date/time in the standard date format
     */
    public String getStart(){
        return start.toString();
    }
    /**
     * @return the end date/time in the standard date format
     */
    public String getEnd(){
        return end.toString();
    }

    public Double getDuration(){
        Double d = new Double( (double) (end.getTime()-start.getTime()) / 1000. );
        return d;
    }
}