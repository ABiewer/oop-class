package app;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

import app.TrieSearchResult;

public class DictionaryTrie {
    private TrieNode root;

    /**
     * Creates a new dictionary
     * @param filepath the file that holds the words on seperate lines
     * @throws FileNotFoundException when the file could not be found
     */
    DictionaryTrie(String filepath) throws FileNotFoundException {
        //make sure the passed file is accessible
        File file = new File(filepath);
        if(!file.exists()){
            throw new FileNotFoundException();
        }

        //Read through the file, adding every line to the dictionary
        Scanner scan = new Scanner(file);
        root = new TrieNode(' ');
        while(scan.hasNextLine()){
            String nextWord = scan.nextLine();
            if(nextWord != ""){
                root.add(nextWord);
            }
        }
        scan.close();
    }

    /**
     * This constructor exists so that there will be a secondary dictionary that gets searched
     * every time a word is passed from the BoggleGUI to the Board to search for a word
     * in the board.
     * @param allwords a string of all the words that are going to be added to the dictionary seperated
     * by spaces
     * @param a a useless variable to differentiate this constructor from the one that takes a file path
     */
    DictionaryTrie(String allwords, boolean a){
        root = new TrieNode(' ');
        String[] sArray = allwords.split(" ");
        for (String s : sArray) {
            if(s!=""){
                root.add(s);
            }
        }
    }

    /**
     * Searches the dictionary for the given word
     * @param word the word to be searched for
     * @return one of the TrieSearchResults that indicates whether the given word was found, not found, or is a partial word
     */
    public TrieSearchResult search(String word){
        TrieSearchResult retval = root.search(word.toLowerCase());
        return retval;
    }
}