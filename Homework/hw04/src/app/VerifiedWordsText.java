package app;

import java.util.ArrayList;

import javax.swing.JLabel;

public class VerifiedWordsText extends JLabel {
    private ArrayList<String> words;

    VerifiedWordsText(){
        super("");
        words = new ArrayList<String>();
    }

    public void add(String word){
        words.add(word);
        String text = generateText();
        this.setText(text);
    }

    private String generateText(){
        StringBuilder sb = new StringBuilder();
        sb.append("<html>");
        for (String s : words) {
            sb.append(s);
            sb.append("<br/>");
        }
        sb.append("</html>");
        return sb.toString();
    }

    public boolean contains(String word){
        return words.contains(word);
    }
}