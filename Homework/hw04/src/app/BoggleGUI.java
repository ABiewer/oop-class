package app;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Collections;

import javax.swing.*;

public class BoggleGUI extends JFrame {
    private BoggleGUI THIS = this;
    private BoggleBoard board;

    private JPanel dicePanelOutline;

    private JPanel dicePanel;
    private ArrayList<ArrayList<JLabel>> diceLabels;

    private JPanel wordSubmitPanel;
    private JLabel enterAWord;
    private JLabel emptyLabel;
    private JTextField wordField;
    private JButton submitButton;

    private JPanel verifiedWordsPanel;
    private JLabel verifiedWordsLabel;
    private JScrollPane verifiedWordsScrollPane;

    private VerifiedWordsText verifiedWordsText;

    private JPanel totalScorePanel;

    private JLabel totalScoreLabel;
    private JLabel totalScoreResult;

    private JButton showAllWordsButton;

    BoggleGUI(String dictionaryFilePath) throws FileNotFoundException {
        super("Boggle");
        board = new BoggleBoard(dictionaryFilePath);
        setUpGUI();
    }

    BoggleGUI(String dictionaryFilePath, String diceOrder) throws FileNotFoundException {
        super("Boggle");
        board = new BoggleBoard(dictionaryFilePath, diceOrder);
        setUpGUI();
    }

    private void setUpGUI() {
        setLayout(new BorderLayout());

        setDicePanelOutline();

        setVerifiedWordsPanel();

        showAllWordsButton = new JButton("End Game and See All Words");
        showAllWordsButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                JDialog f = new JDialog(THIS, "All Words");

                ArrayList<String> allWords = new ArrayList<String>();
                for (String s1 : board.getAllWords()) {
                    allWords.add(s1);
                }
                Collections.sort(allWords);
                String s = "";
                for (String s1 : allWords) {
                    s = s + "\n" + s1;
                }

                JTextArea allWordsTextArea = new JTextArea();
                allWordsTextArea.setText(s);

                JScrollPane allWordsPane = new JScrollPane(allWordsTextArea);
                f.add(allWordsPane);

                f.setSize(300, 600);
                f.setVisible(true);
            }
        });
        add(showAllWordsButton, BorderLayout.PAGE_END);

        setSize(400, 300);
        setLocation(100, 100);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setVisible(true);
    }

    private void setDicePanelOutline() {
        dicePanelOutline = new JPanel(new BorderLayout());

        dicePanel = new JPanel(new GridLayout(5, 5));
        diceLabels = new ArrayList<ArrayList<JLabel>>();

        ArrayList<ArrayList<BoggleDie>> dice = board.getDiceList();
        for (int i = 0; i < 5; i++) {
            diceLabels.add(new ArrayList<JLabel>());
            for (int j = 0; j < 5; j++) {
                JLabel b = new JLabel(dice.get(i).get(j).getShownValue(), SwingConstants.CENTER);
                diceLabels.get(i).add(b);
                dicePanel.add(b);
            }
        }

        wordSubmitPanel = new JPanel(new GridLayout(2, 2));

        enterAWord = new JLabel("Enter a Word: ");
        emptyLabel = new JLabel("");
        wordField = new JTextField();
        submitButton = new JButton("Submit");

        submitButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String word = wordField.getText().trim();
                if (word == "") {
                    emptyLabel.setText("Please Enter a Word");
                    return;
                }
                TrieSearchResult result = board.search(word);
                if (result == TrieSearchResult.NOT_FOUND) {
                    emptyLabel.setText("Word Not Found in Dictionary");
                } else if (result == TrieSearchResult.PARTIAL) {
                    emptyLabel.setText("Result is a Partial Word");
                } else if (verifiedWordsText.contains(word)) {
                    emptyLabel.setText("Word Already Entered");
                } else if (word.length() > 3) {
                    emptyLabel.setText("Word Entered");
                    verifiedWordsText.add(word);
                    Integer i = new Integer( Integer.parseInt(  totalScoreResult.getText()  ) + word.length() - 3 );
                    totalScoreResult.setText(i.toString());
                    verifiedWordsScrollPane.revalidate();
                } else {
                    emptyLabel.setText("Word is Too Short");
                }
            }
        });

        wordSubmitPanel.add(enterAWord);
        wordSubmitPanel.add(emptyLabel);
        wordSubmitPanel.add(wordField);
        wordSubmitPanel.add(submitButton);

        dicePanelOutline.add(dicePanel, BorderLayout.CENTER);
        dicePanelOutline.add(wordSubmitPanel, BorderLayout.PAGE_END);

        this.add(dicePanelOutline, BorderLayout.CENTER);
    }

    private void setVerifiedWordsPanel() {
        verifiedWordsPanel = new JPanel(new BorderLayout());

        verifiedWordsLabel = new JLabel("Found Words:");

        verifiedWordsText = new VerifiedWordsText();
        verifiedWordsScrollPane = new JScrollPane(verifiedWordsText);

        totalScorePanel = new JPanel(new GridLayout(1, 2));
        totalScoreLabel = new JLabel("Total Score: ");
        totalScoreResult = new JLabel("0");
        totalScorePanel.add(totalScoreLabel);
        totalScorePanel.add(totalScoreResult);

        verifiedWordsPanel.add(verifiedWordsLabel, BorderLayout.PAGE_START);
        verifiedWordsPanel.add(verifiedWordsScrollPane, BorderLayout.CENTER);
        verifiedWordsPanel.add(totalScorePanel, BorderLayout.PAGE_END);

        THIS.add(verifiedWordsPanel, BorderLayout.LINE_END);
    }
}