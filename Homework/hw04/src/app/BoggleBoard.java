package app;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.Random;

public class BoggleBoard {
    private DictionaryTrie dict;
    private ArrayList<ArrayList<BoggleDie>> dice;
    private HashSet<String> allWords;
    private DictionaryTrie onlyThisBoardDict;

    /**
     * Creates a BoggleBoard with a randomly selected dice layout
     * @param dictionaryFileName filepath to the dictionary file
     * @throws FileNotFoundException when the dictionary file could not be found/opened
     */
    BoggleBoard(String dictionaryFileName) throws FileNotFoundException{
        dice = new ArrayList<ArrayList<BoggleDie>>();
        dict = new DictionaryTrie(dictionaryFileName);
        for(int i = 0;i<5;i++){
            dice.add(new ArrayList<BoggleDie>());
        }
        generateRandomBoard();
        generateAllWordsList();
        generateThisBoardDictionary();
    }

    /**
     * Creates a BoggleBoard with a fixed order for the dice layout
     * @param dictionaryFileName the filepath to the dictionary file
     * @param diceOrder the space-seperated list of dice values
     * @throws FileNotFoundException when the dictionary file could not be found/opened
     */
    BoggleBoard(String dictionaryFileName, String diceOrder) throws FileNotFoundException{
        dice = new ArrayList<ArrayList<BoggleDie>>();
        dict = new DictionaryTrie(dictionaryFileName);
        for(int i = 0;i<5;i++){
            dice.add(new ArrayList<BoggleDie>());
        }
        generateFixedBoard(diceOrder);
        generateAllWordsList();
        generateThisBoardDictionary();
    }

    /**
     * Generates a boggle board dice order with a specific layout of the dice
     * @param diceOrder the order the dice should appear
     */
    private void generateFixedBoard(String diceOrder){
        int i = 0;
        while(diceOrder.length()!=0){
            String tmp = diceOrder.substring(0,1);
            diceOrder = diceOrder.substring(1);
            if(diceOrder.length() == 0){
                //diceOrder is now empty
            }
            else if(diceOrder.charAt(0)!=' '){
                //dice Order has an extra character
                tmp = tmp + diceOrder.substring(0,1);
                diceOrder = diceOrder.substring(2);
            }
            else{
                //there is a space between the characters
                diceOrder = diceOrder.substring(1);
            }
            BoggleDie b = new BoggleDie(tmp);
            dice.get(i/5).add(b);
            i++;
        }
    }

    /** 
     * Generates a boggle board dice order with a randomized layout of the dice
     */
    private void generateRandomBoard(){
        ArrayList<ArrayList<String>> stringlist = new ArrayList<ArrayList<String>>();
        String[][] arrays= {{ "N", "S", "C", "T", "E", "C" },
        { "A", "E", "A", "E", "E", "E" },
        { "H", "H", "L", "R", "O", "D" },
        { "O", "R", "W", "V", "G", "R" },
        { "S", "P", "R", "I", "Y", "Y" },
        { "S", "U", "E", "N", "S", "S" },
        { "M", "E", "A", "U", "E", "G" },
        { "S", "E", "P", "T", "I", "C" },
        { "D", "H", "H", "O", "W", "N" },
        { "L", "E", "P", "T", "I", "S" },
        { "S", "T", "L", "I", "E", "I" },
        { "A", "R", "S", "I", "Y", "F" },
        { "T", "E", "T", "I", "I", "I" },
        { "O", "T", "T", "T", "M", "E" },
        { "N", "M", "N", "E", "G", "A" },
        { "N", "N", "E", "N", "A", "D" },
        { "O", "U", "O", "T", "T", "O" },
        { "B", "Z", "J", "B", "X", "K" },
        { "A", "A", "F", "A", "S", "R" },
        { "T", "O", "O", "U", "W", "N" },
        { "O", "T", "H", "D", "D", "N" },
        { "R", "A", "A", "S", "F", "I" },
        { "H", "O", "D", "R", "L", "N" },
        { "E", "E", "E", "E", "A", "M" },
        { "He", "Qu", "Th", "In", "Er", "An" }};
        for(int i = 0;i<25;i++){
            stringlist.add(new ArrayList<String>(Arrays.asList(arrays[i])));
        }

        for(int i = 0;i<25;i++){
            Random random = new Random();
            int arrayindex = random.nextInt(stringlist.size());
            int j = random.nextInt(6);
            dice.get(i/5).add(new BoggleDie(stringlist.get(arrayindex).get(j)));
            stringlist.remove(arrayindex);
        }
    }

    /**
     * Generates the internal list that gets all of the words on the boggle board
     */
    private void generateAllWordsList(){
        allWords = new HashSet<String>();
        //loop through each die
        for(int i = 0;i<5;i++){
            for(int j = 0;j<5;j++){
                BoggleDie firstDie = dice.get(i).get(j);
                TrieSearchResult result = dict.search(firstDie.getValue());
                if(result==TrieSearchResult.NOT_FOUND){
                    break;
                }
                //for each spot in the board...
                for(int k = i-1;k<i+2;k++){
                    if(k<0||k>4){
                        continue;
                    }
                    for(int l = j-1;l<j+2;l++){
                        if(l<0||l>4){
                            continue;
                        }
                        ArrayList<BoggleDie> previouslyVisited = new ArrayList<BoggleDie>();
                        previouslyVisited.add(firstDie);
                        String startWord = firstDie.getValue();
                        recursiveAllWords(startWord,previouslyVisited,k,l);
                    }
                }
            }
        }
    }

    /**
     * The recursive function that sets the internal list of boggle board words
     * @param startWord the string containing the letters up to this point
     * @param previouslyVisited the arraylist of boggle dice which have already been visited, so you don't visit them again
     * @param k the x location of the current die to be checked
     * @param l the y location of the current die to be checked
     */
    private void recursiveAllWords(String startWord, ArrayList<BoggleDie> previouslyVisited, int x, int y){
        BoggleDie thisDie = dice.get(x).get(y);
        for (BoggleDie var : previouslyVisited) {
            if(thisDie == var){
                return;
            }
        }
        startWord = startWord + thisDie.getValue();
        TrieSearchResult result = dict.search(startWord);
        if(result == TrieSearchResult.FOUND && startWord.length()>3){
            allWords.add(startWord);
        }
        else if(result == TrieSearchResult.NOT_FOUND){
            return;
        }
        for(int k = x-1;k<x+2;k++){
            if(k<0||k>4)
                continue;
            for(int l = y-1;l<y+2;l++){
                if(l<0||l>4)
                    continue;
                previouslyVisited.add(thisDie);
                recursiveAllWords(startWord,previouslyVisited,k,l);
            }
        }
    }

    /**
     * Gets the HashSet of all the words on the boggle board.
     * @return a HashSet of Strings for all the words
     */
    public HashSet<String> getAllWords(){
        return allWords;
    }

    /**
     * Builds a string to represent the dice on the board
     * @return a string of 5x5 dice from the boggle board
     */
    public String getDiceOrder(){
        StringBuilder sb = new StringBuilder();
        for(int i = 0;i<5;i++){
            for(int j = 0;j<5;j++){
                sb.append(dice.get(i).get(j).getValue());
                sb.append(" ");
            }
            sb.append("\n");
        }
        return sb.toString();
    }

    public ArrayList<ArrayList<BoggleDie>> getDiceList(){
        return dice;
    }

    public TrieSearchResult search(String word){
        return onlyThisBoardDict.search(word);
    }

    private void generateThisBoardDictionary(){
        ArrayList<String> allWordsArray = new ArrayList<String>();
            for (String s1 : allWords) {
                allWordsArray.add(s1);
            }
            Collections.sort(allWordsArray);
            String s = "";
            for (String s1 : allWords) {
                s = s + " " + s1;
            }
            onlyThisBoardDict = new DictionaryTrie(s, true);
    }
}