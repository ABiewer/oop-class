package app;

import java.util.ArrayList;

import app.TrieSearchResult;

public class TrieNode {
    private char letter;
    private boolean isWord;
    private ArrayList<TrieNode> subTries;
    
    /**
     * Creates a Trie Node
     */
    TrieNode(char letter){
        subTries = new ArrayList<TrieNode>();
        this.letter = letter;
        isWord = false;
    }

    /**
     * Adds the word to the trie
     * 
     * @param value the word to add to the trie
     */
    public void add(String value){
        if(value.length()==0){
            isWord = true;
            return;
        }
        TrieNode n = null;

        char nextLetter = value.charAt(0);

        for (TrieNode node : subTries) {
            if(node.getLetter()==nextLetter){
                n = node;
                break;
            }
        }
        if(n==null){
            n = new TrieNode(nextLetter);
            subTries.add(n);
        }
        n.add(value.substring(1));
    }

    /**
     * gets the letter of the trie
     * @return the letter of the trie
     */
    public char getLetter(){
        return letter;
    }

    /**
     * Searches through the nodes to find the word, or figure out if it's a partial word
     * @param word the search parameter
     * @return the result of the search 
     */
    public TrieSearchResult search(String word){
        //If we've hit the end of the search, determine if it was found or continues
        if(word.length()==0){
            if(isWord == true){
                return TrieSearchResult.FOUND;
            }
            else{
                return TrieSearchResult.PARTIAL;
            }
        }
        char nextLetter = word.charAt(0);
        TrieSearchResult retval = TrieSearchResult.NOT_FOUND;
        for (TrieNode node : subTries) {
            if(node.getLetter() == nextLetter){
                retval = node.search(word.substring(1));
                break;
            }
        }
        return retval;
    }
}