package app;

public enum TrieSearchResult{
    FOUND,
    NOT_FOUND,
    PARTIAL
}