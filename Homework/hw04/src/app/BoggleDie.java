package app;



public class BoggleDie{
    private String value;
    private String shownValue;
    BoggleDie(String value){
        setValue(value.toLowerCase());
        setShownValue(value);
    }

    public String getValue(){
        return value;
    }
    private void setValue(String value){
        this.value = value;
    }
    
    public String getShownValue(){
        return shownValue;
    }
    private void setShownValue(String shownValue){
        this.shownValue = shownValue;
    }
}