package app;

import javax.swing.JButton;

public class PixelButton extends JButton{
    private int myX;
    private int myY;
    private int r;
    private int g;
    private int b;

    PixelButton(int myX,int myY){
        super("");
        setMyX(myX);
        setMyY(myY);
        setR(255);
        setG(255);
        setB(255);
    }

    private void setMyY(int myY){
        if(myY<0){
            this.myY = 0;
        }
        else{
            this.myY = myY;
        }
    }
    public int getMyY(){
        return myY;
    }
    
    public void setMyX(int myX){
        if(myX<0){
            this.myX = 0;
        }
        else{
            this.myX = myX;
        }
    }
    public int getMyX(){
        return myX;
    }

    public void setR(int r){
        if(r<0||r>255){
            this.r = 0;
        }
        else{
            this.r = r;
        }
    }
    public int getR(){
        return r;
    }
    
    public void setG(int g){
        if(g<0||g>255){
            this.g = 0;
        }
        else{
            this.g = g;
        }
    }
    public int getG(){
        return g;
    }
    
    public void setB(int b){
        if(b<0||b>255){
            this.b = 0;
        }
        else{
            this.b = b;
        }
    }
    public int getB(){
        return b;
    }
}