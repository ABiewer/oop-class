package app;
import java.util.ArrayList;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

public class Icon{
    private ArrayList<ArrayList<Pixel>> icon;
    private int row;
    private int col;
    public Icon(){ //default constructor builds a 40x40 array of pixels
        buildIcon(40,40);
        row = 40;
        col = 40;
    }
    public Icon(int row, int col){
        buildIcon(row,col);
        this.row = row;
        this.col = col;
    }
    private void buildIcon(int row, int col){
        //add the necessary number of pixels to the arrayLists
        icon = new ArrayList<ArrayList<Pixel>>();
        for(int i = 0;i<row;i++){
            icon.add(new ArrayList<Pixel>());
            for(int j = 0;j<col;j++){
                icon.get(i).add(new Pixel());
            }
        }
    }



    private Pixel getPixel(int row, int col){
        //value checking will be done by all public functions which call private ones
        return icon.get(row).get(col);
    }



    public String toString(){
        //formats the arraylist into a grid of hex values in a string
        String ret = "";
        for(int i = 0;i<row;i++){
            for(int j = 0;j<col;j++){
                ret = ret + getPixel(i,j).printHex() + " ";
            }
            ret = ret + "\n";
        }
        return ret;
    }



    public String getRGB(int row, int col){
        //gets the printhex() from the pixel, showing the rgb there
        if(row>this.row||row<0||col>this.col||col<0){
            return null;
        }
        Pixel tmp = getPixel(row,col);
        return tmp.printHex();
    }
    public void setRGB(int row, int col, int r, int g, int b){
        //sets the rgb value of a specific pixel
        if(row>this.row||row<0||col>this.col||col<0){
            return;
        }
        Pixel tmp = getPixel(row, col);
        tmp.setRed(r);  //all the methods check for bad values
        tmp.setGreen(g);
        tmp.setBlue(b);
    }



    public void toBitMap(String filename){
        ArrayList<Byte> bytes = new ArrayList<Byte>();
        //bit maps have a file header, info header, and pixel data

        //File Header
        bytes.add((byte)'B');           //first 2 bytes are 'BM'
        bytes.add((byte)'M');
        int numbytes = 0;//Set at end of file
        bytes.add((byte)numbytes);      //size of entire file in bytes
        bytes.add((byte)(numbytes>>8));
        bytes.add((byte)(numbytes>>16));
        bytes.add((byte)(numbytes>>24));
        for(int i = 0;i<4;i++){         //4 bytes unused
            bytes.add((byte)0);
        }
        bytes.add((byte)54);            //offset into pixel data
        bytes.add((byte)0);
        bytes.add((byte)0);
        bytes.add((byte)0);

        //Info Header
        bytes.add((byte)40);            //size of info header
        bytes.add((byte)0);
        bytes.add((byte)0);
        bytes.add((byte)0);
        int width = col;
        int height = row;
        bytes.add((byte)width);         //width of picture in pixels
        bytes.add((byte)(width>>8));
        bytes.add((byte)(width>>16));
        bytes.add((byte)(width>>24));
        bytes.add((byte)height);        //height of picture in pixels
        bytes.add((byte)(height>>8));
        bytes.add((byte)(height>>16));
        bytes.add((byte)(height>>24));
        bytes.add((byte)1);             //number of planes
        bytes.add((byte)0);
        bytes.add((byte)24);            //number of bits per pixel
        bytes.add((byte)0);
        for(int i = 0;i<4;i++){         //4 bytes for compression
            bytes.add((byte)0);
        }
        int pixelDataSize = (3*width*height); //3 bytes per color, times number of pixels total
        bytes.add((byte)pixelDataSize); //4 bytes for the pixel data size in bytes
        bytes.add((byte)(pixelDataSize>>8));
        bytes.add((byte)(pixelDataSize>>16));
        bytes.add((byte)(pixelDataSize>>24));
        for(int i = 0;i<16;i++){         //16 bytes unused
            bytes.add((byte)0);
        }
        
        //pixel data
        int padding = icon.get(0).size()%4;
        for(int i = icon.size()-1;i>=0;i--){    //start at last row, move left->right, then up
            for(int j = 0;j<icon.get(i).size();j++){
                Pixel p = icon.get(i).get(j);
                bytes.add((byte) p.getBlue());
                bytes.add((byte) p.getGreen());
                bytes.add((byte) p.getRed());
            }
            for(int j = 0;j<padding;j++){
                bytes.add((byte)0);
            }
        }

        
        //now that all the bytes have been added, update the size of the file
        numbytes = bytes.size();
        bytes.set(2,(byte)numbytes);      //size of entire file in bytes
        bytes.set(3,(byte)(numbytes>>8));
        bytes.set(4,(byte)(numbytes>>16));
        bytes.set(5,(byte)(numbytes>>24));

        //convert the ArrayList<Byte> to byte[]
        byte[] out = new byte[bytes.size()];
        for (int i = 0; i < bytes.size(); i++) {
            out[i] = bytes.get(i);
        }
        
        //file output to a bmp, the input string should be a .bmp 
        FileOutputStream fop = null;
        File file;
        try{
            file = new File(filename);
            fop = new FileOutputStream(file);
            if (!file.exists()) {
                file.createNewFile();
            }
            fop.write(out);
            fop.flush();
            fop.close();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (fop != null) {
                    fop.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        /*for(int i = 0;i<out.length;i++){
            if(i==54){System.out.print("\n");}
            System.out.print((int)out[i] + " ");
        }*/
    }
}