package app;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.util.ArrayList;

import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

public class IconEditorFrame extends JFrame {
    private IconEditorFrame THIS = this;
    private Icon icon;
    private ArrayList<ArrayList<PixelButton>> pixels;
    private ArrayList<PrevColorButton> prevColorButtons;
    private JPanel pixelPanel;
    private JPanel sliderPanel;
    private JPanel previewColor;
    private JPanel prevColors;
    private JPanel advancedAndSubmit;
    private JSlider redSlider;
    private JSlider greenSlider;
    private JSlider blueSlider;
    private boolean advanced;
    private int row;
    private int col;

    IconEditorFrame(int row, int col) {
        //First, Create the Icon to be made into a bitmap at the end.
        super("Icon Editor");
        if (row < 1) {
            row = 1;
        }
        if (col < 1) {
            col = 1;
        }
        icon = new Icon(row, col);
        this.row = row;
        this.col = col;

        advanced = false;
        pixels = new ArrayList<ArrayList<PixelButton>>();
        prevColorButtons = new ArrayList<PrevColorButton>();

        //The layout of the whole frame
        this.setLayout(new BorderLayout());

        /* 
         * The first frame will have all the buttons and will change colors.
         * Also needs to update the icon when changed. 
         */
        pixelPanel = new JPanel();
        pixelPanel.setLayout(new GridLayout(row, col));
        if(row>=col){
            int pixelLen = 500/row;
            pixelPanel.setPreferredSize(new Dimension(pixelLen*row,pixelLen*col));
        }
        else{
            int pixelLen = 500/col;
            pixelPanel.setPreferredSize(new Dimension(pixelLen*row,pixelLen*col));
        }
        for (int i = 0; i < row; i++) {
            pixels.add(new ArrayList<PixelButton>());
            for (int j = 0; j < col; j++) {

                PixelButton b = new PixelButton(i,j);
                pixels.get(i).add(b);
                b.setBackground(new Color(255,255,255));
                b.addActionListener(new ActionListener(){
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        if(!advanced){

                            ((PixelButton)e.getSource()).setR(redSlider.getValue());
                            ((PixelButton)e.getSource()).setG(greenSlider.getValue());
                            ((PixelButton)e.getSource()).setB(blueSlider.getValue());

                            int red = b.getR();
                            int green = b.getG();
                            int blue = b.getB();

                            b.setBackground(new Color(red, green, blue));

                            icon.setRGB(b.getMyX(), b.getMyY(), red, green, blue);
                            modifyPreviousColors(red, green, blue);
                        }
                        else{
                            //'this' in this context is the action listener, so I made a variable 
                            //for the 'this' from the IconEditorFrame
                            PixelButton pb = (PixelButton)e.getSource();
                            JDialog advancedBox = new JDialog(THIS,"advanced");
                            advancedBox.setSize(500,500);
                            advancedBox.setLocation(100,100);
                            advancedBox.setLayout(new BorderLayout());
                            
                            JPanel ButtonPanel = new JPanel(new GridLayout(1,2));
                            JPanel advancedDetails = new JPanel();

                            JButton button1 = new JButton("Rectangular");
                            JButton button2 = new JButton("Import");

                            button1.addActionListener(new ActionListener(){
                                @Override
                                public void actionPerformed(ActionEvent e) {
                                    advanced1(advancedDetails, pb, advancedBox);
                                }
                            });
                            button2.addActionListener(new ActionListener(){
                                @Override
                                public void actionPerformed(ActionEvent e){
                                    advanced2(advancedDetails,pb,advancedBox);
                                }
                            });
                            ButtonPanel.add(button1);
                            ButtonPanel.add(button2);
                            
                            advancedBox.add(ButtonPanel,BorderLayout.PAGE_START);


                            advanced1(advancedDetails, pb, advancedBox);
                            //this stuff goes into advanced1
                            
                            //end advanced1

                            advancedBox.setVisible(true);
                        }
                    }
                });
                pixelPanel.add(b);
            }
        }
        pixelPanel.setVisible(true);
        add(pixelPanel, BorderLayout.PAGE_START);

        /*
         * The second frame holds the rgb labels and sliders
         * 
         */
        sliderPanel = new JPanel();
        sliderPanel.setLayout(new GridBagLayout());
        GridBagConstraints c = new GridBagConstraints();
        sliderPanel.setPreferredSize(new Dimension(150,250));

        ChangeListener sliderChange = new ChangeListener(){
            @Override
            public void stateChanged(ChangeEvent e) {
                int red = redSlider.getValue();
                int green = greenSlider.getValue();
                int blue = blueSlider.getValue();

                previewColor.setBackground(new Color(red,green,blue));
            }
        };

        redSlider = new JSlider(0,255);
        redSlider.setPreferredSize(new Dimension(120,83));
        redSlider.addChangeListener(sliderChange);
        greenSlider = new JSlider(0,255);
        greenSlider.setPreferredSize(new Dimension(120,83));
        greenSlider.addChangeListener(sliderChange);
        blueSlider = new JSlider(0,255);
        blueSlider.setPreferredSize(new Dimension(120,84));
        blueSlider.addChangeListener(sliderChange);
        JLabel redLabel = new JLabel("R");
        redLabel.setPreferredSize(new Dimension(30,83));
        JLabel greenLabel = new JLabel("G");
        greenLabel.setPreferredSize(new Dimension(30,83));
        JLabel blueLabel = new JLabel("B");
        blueLabel.setPreferredSize(new Dimension(30,84));

        c.weighty = 1;
        c.weightx = 1;
        c.gridy = 0;
        c.fill = GridBagConstraints.HORIZONTAL;

        c.gridx = 0;
        c.gridwidth = 1;
        sliderPanel.add(redLabel,c);

        c.gridx = 1;
        c.gridwidth = 4;
        sliderPanel.add(redSlider,c);

        c.gridy = 1;

        c.gridx = 0;
        c.gridwidth = 1;
        sliderPanel.add(greenLabel,c);

        c.gridx = 1;
        c.gridwidth = 4;
        sliderPanel.add(greenSlider,c);

        c.gridy = 2;

        c.gridx = 0;
        c.gridwidth = 1;
        sliderPanel.add(blueLabel,c);

        c.gridx = 1;
        c.gridwidth = 4;
        sliderPanel.add(blueSlider,c);
        
        add(sliderPanel, BorderLayout.LINE_START);

        /*
         * The middle component holds the current color
         * 
         */
        previewColor = new JPanel();
        previewColor.setBackground(new Color(127,127,127));
        previewColor.setPreferredSize(new Dimension(250,250));
        add(previewColor, BorderLayout.CENTER);


        /*
         * The 4th component holds 5 of the most recently used colors
         * 
         */
        prevColors = new JPanel();
        prevColors.setLayout(new GridLayout(5,1));
        prevColors.setPreferredSize(new Dimension(100,250));
        for(int i = 0;i<5;i++){
            PrevColorButton b = new PrevColorButton(i);
            b.setBackground(new Color(255,255,255));
            b.setPreferredSize(new Dimension(100,50));
            b.addActionListener(new ActionListener(){
                @Override
                public void actionPerformed(ActionEvent e) {
                    redSlider.setValue(((PrevColorButton)e.getSource()).getR());
                    greenSlider.setValue(((PrevColorButton)e.getSource()).getG());
                    blueSlider.setValue(((PrevColorButton)e.getSource()).getB());
                }
            });
            prevColorButtons.add(b);
            prevColors.add(b);
        }
        add(prevColors, BorderLayout.LINE_END);

        /*
         * The final element holds the advanced button and Submit
         * 
         */
        advancedAndSubmit = new JPanel();
        advancedAndSubmit.setLayout(new GridLayout(1, 2));
        advancedAndSubmit.setPreferredSize(new Dimension(500,100));
        JCheckBox advancedBox = new JCheckBox("advanced");
        advancedBox.setPreferredSize(new Dimension(250,100));
        advancedBox.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent e) {
                advanced = !advanced;
            }
        });
        advancedAndSubmit.add(advancedBox);
        GenerateIconButton b = new GenerateIconButton();
        b.setPreferredSize(new Dimension(250,100));
        b.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent e) {
                ((GenerateIconButton)e.getSource()).onClick(icon);
            }
        });
        advancedAndSubmit.add(b);

        add(advancedAndSubmit, BorderLayout.PAGE_END);

        /*
         * 
         * 
         */
        setSize(500, 850);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setLocation(400, 0);
        setVisible(true);
    }


    private void modifyPreviousColors(int red, int green, int blue){
        int pos = 4;
        for(int i = 0;i<5;i++){
            PrevColorButton pb = prevColorButtons.get(i);
            int comparison = red+1000*green+1000*1000*blue;
            if(pb.getRGB() == comparison){
                if(i==0){
                    return;
                }
                else{
                    pos = i;
                    break;
                }
            }
        }

        PrevColorButton nextButton;
        for(int i = pos;i>0;i--){
            nextButton = prevColorButtons.get(i-1);
            prevColorButtons.get(i).setBackground(nextButton.getBackground());
            prevColorButtons.get(i).setRGB(nextButton.getBackground());
        }
        prevColorButtons.get(0).setBackground(new Color(red,green,blue));
        prevColorButtons.get(0).setRGB(new Color(red,green,blue));
    }

    private void advanced1(JPanel advancedDetails, PixelButton pb, JDialog advancedBox){
        advancedDetails.removeAll();
        advancedDetails.setLayout(new GridLayout(3,2));
        JLabel a1 = new JLabel("Rows: ");
        JLabel a2 = new JLabel("Cols: ");
        JLabel empty = new JLabel("");
        JTextField b1 = new JTextField();
        JTextField b2 = new JTextField();
        JButton confirm = new JButton("Confirm");
        confirm.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent e) {
                try{
                    int rows = Integer.parseInt(b1.getText().trim());
                    int cols = Integer.parseInt(b2.getText().trim());
                    if(rows<1){
                        throw new NumberFormatException("Invalid input");
                    }
                    if(cols<1){
                        throw new NumberFormatException("Invalid input");
                    }
                    //put the colors here, call modify colors after loop
                    int red = redSlider.getValue();
                    int green = greenSlider.getValue();
                    int blue = blueSlider.getValue();
                    for(int i = pb.getMyX();i<pb.getMyX()+rows;i++){
                        if(i>=THIS.row){
                            break;
                        }
                        for(int j = pb.getMyY();j<pb.getMyY()+cols;j++){
                            if(j>=THIS.col){
                                break;
                            }
                            else{
                                PixelButton tmpButton = pixels.get(i).get(j);
                                tmpButton.setR(red);
                                tmpButton.setG(green);
                                tmpButton.setB(blue);

                                tmpButton.setBackground(new Color(red, green, blue));

                                icon.setRGB(i, j, red, green, blue);
                            }
                        }
                    }
                    modifyPreviousColors(red, green, blue);
                    advancedBox.dispose();
                }
                catch(NumberFormatException err){
                    System.out.println("Err: " + err);
                }
            }
        });

        advancedDetails.add(a1);
        advancedDetails.add(b1);
        advancedDetails.add(a2);
        advancedDetails.add(b2);
        advancedDetails.add(empty);
        advancedDetails.add(confirm);
        
        advancedBox.add(advancedDetails,BorderLayout.CENTER);
        advancedBox.revalidate();
        advancedBox.repaint();
    }

    private void advanced2(JPanel advancedDetails, PixelButton pb, JDialog advancedBox){
        advancedDetails.removeAll();
        advancedDetails.setLayout(new BorderLayout());

        FilePreviewPanel filePreview = new FilePreviewPanel();
        JLabel previewText = new JLabel("I couldn't get the preview to work. the import still works though.");
        filePreview.add(previewText);
        filePreview.setPreferredSize(new Dimension(500,450));
        filePreview.setBounds(0,0,500,450);

        JPanel fileChooser = new JPanel();
        fileChooser.setLayout(new GridLayout(1,2));
        ImportIconButton chooseFile = new ImportIconButton();
        chooseFile.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent e) {
                chooseFile.onClick(filePreview);
                filePreview.repaint();
                filePreview.revalidate();
            }
        });

        ConfirmButton confirmButton = new ConfirmButton();
        confirmButton.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent e) {
                BufferedImage image = filePreview.getImage();
                if(image != null){
                    for(int i = pb.getMyX();i<pb.getMyX()+image.getWidth();i++){
                        if(i>=THIS.row){
                            break;
                        }
                        for(int j = pb.getMyY();j<pb.getMyY()+image.getHeight();j++){
                            if(j>=THIS.col){
                                break;
                            }
                            else{
                                PixelButton tmpButton = pixels.get(i).get(j);
                                //I have absolutely no idea why this isn't working. 
                                int color = image.getRGB(i-pb.getMyX(),j-pb.getMyY());
                                int blue = color & 0x000000ff;
                                int green = (color & 0x0000ff00) >> 8;
                                int red = (color & 0x00ff0000) >> 16;
                                tmpButton.setR(red);
                                tmpButton.setG(green);
                                tmpButton.setB(blue);

                                tmpButton.setBackground(new Color(red, green, blue));

                                icon.setRGB(i, j, red, green, blue);
                            }
                        }
                    }
                    advancedBox.dispose();
                }
            }
        });


        fileChooser.add(chooseFile);
        fileChooser.add(confirmButton);

        advancedDetails.add(fileChooser, BorderLayout.PAGE_START);
        advancedDetails.add(filePreview, BorderLayout.CENTER);

        advancedBox.revalidate();
        advancedBox.repaint();
    }
}