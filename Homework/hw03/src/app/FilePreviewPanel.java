package app;

//import java.awt.Graphics;
import java.awt.image.BufferedImage;
import javax.swing.*;

public class FilePreviewPanel extends JPanel{
    private BufferedImage image;

    public FilePreviewPanel() {
        image = null;
    }

    /**
     * @param image the image to set
     */
    public void setImage(BufferedImage image) {
        this.image = image;
        repaint();
    }

    public BufferedImage getImage(){
        return image;
    }
}