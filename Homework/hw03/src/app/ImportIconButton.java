package app;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.*;

public class ImportIconButton extends JButton {
    ImportIconButton(){
        super("Import");
    }

    public void onClick(FilePreviewPanel filePreview){
        JFileChooser fc = new JFileChooser();
        BufferedImage image = null;
        int retval = fc.showOpenDialog(this);
        if (retval == JFileChooser.APPROVE_OPTION) {
            File file = fc.getSelectedFile();
            try {
                image = ImageIO.read(file);
            } catch (IOException e) {

            }
            filePreview.setImage(image);
        }
    }
}