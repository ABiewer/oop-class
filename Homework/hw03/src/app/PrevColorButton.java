package app;

import java.awt.Color;

import javax.swing.JButton;

public class PrevColorButton extends JButton{
    private int y;
    private int r;
    private int g;
    private int b;

    PrevColorButton(int y){
        super("");
        setY(y);
        setR(255);
        setG(255);
        setB(255);
    }

    private void setY(int y){
        if(y<0){
            this.y = 0;
        }
        else{
            this.y = y;
        }
    }
    public int getMyY(){
        return y;
    }
    
    public void setR(int r){
        if(r<0||r>255){
            this.r = 0;
        }
        else{
            this.r = r;
        }
    }
    public int getR(){
        return r;
    }
    
    public void setG(int g){
        if(g<0||g>255){
            this.g = 0;
        }
        else{
            this.g = g;
        }
    }
    public int getG(){
        return g;
    }
    
    public void setB(int b){
        if(b<0||b>255){
            this.b = 0;
        }
        else{
            this.b = b;
        }
    }
    public int getB(){
        return b;
    }
    
    public int getRGB(){
        return r+1000*g+1000*1000*b;
    }

    public void setRGB(Color color){
        setR(color.getRed());
        setG(color.getGreen());
        setB(color.getBlue());
    }
}