package app;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;


public class IconFrame extends JFrame implements ActionListener {
    private Icon icon;

    private JLabel rowLabel;
    private JLabel colLabel;
    private JLabel emptyLabel;

    private JTextField row;
    private JTextField col;
    private JButton submitButton;

    private JPanel formPanel;

    IconFrame(){
        super("Enter the Icon's dimensions.");

        setUpGUI();
    }

    private void setUpGUI(){
        rowLabel = new JLabel("Number of rows: ");
        colLabel = new JLabel("Number of cols: ");
        emptyLabel = new JLabel("");

        row = new JTextField(10);
        col = new JTextField(10);

        submitButton = new JButton("Submit");
        submitButton.addActionListener(this);


        formPanel = new JPanel();
        formPanel.setLayout(new GridLayout(3,2));
        formPanel.add(rowLabel);
        formPanel.add(row);
        formPanel.add(colLabel);
        formPanel.add(col);
        formPanel.add(emptyLabel);
        formPanel.add(submitButton);

        setLayout(new BorderLayout());

        add(formPanel, BorderLayout.CENTER);

        setSize(400,300);

        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        setLocation(100,100);

        setVisible(true);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        try{
            int rowNum = Integer.parseInt(row.getText().trim());
            int colNum = Integer.parseInt(col.getText().trim());
            if(rowNum<1){
                rowNum = 1;
            }
            if(colNum<1){
                colNum = 1;
            }
            
            JFrame iFrame = new IconEditorFrame(rowNum,colNum);
        }
        catch(NumberFormatException err){
            System.out.println("Error: " + err);
        }
    }
}