package app;

import java.io.File;

import javax.swing.JButton;
import javax.swing.JFileChooser;

public class GenerateIconButton extends JButton{
    GenerateIconButton(){
        super("Generate Icon");
    }

    public void onClick(Icon icon){
        JFileChooser fc = new JFileChooser();
        fc.showSaveDialog(this);
        File f = fc.getSelectedFile();
        String path = f.getAbsolutePath();
        icon.toBitMap(path);
    }
}