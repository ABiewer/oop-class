package app;

public class Pixel{
    private int rgb = 0;
    //rgb is set up as 8 bits 0, 8 bits for red, 8 bits for green, and 8 bits for blue
    //ie:   00000000 11111111 11111111 11111111
    //      0        red     green    blue

    private boolean debug = false;
    public Pixel(){ //default constructor is a white pixel(255,255,255)
        rgb = 0x00ffffff;
    }
    public Pixel(int r,int g,int b){ //constructor for a three-int pair of rgb
        //values are checked by set functions
        setBlue(b);
        setGreen(g);
        setRed(r);
    }
    public Pixel(int r,int g,int b, boolean debug){ //constructor for a three-int pair of rgb
        //values are checked by set functions
        setBlue(b);
        setGreen(g);
        setRed(r);
        this.debug = debug;
    }


    public int getBlue(){
        //gets the blue component of rgb
        int mask=0x000000ff; //0x is hexadecimal
        int tmp = rgb&mask; //bitwise getting only the blue component
        //no shifting necessary
        return tmp;
    }
    public int getGreen(){
        int mask = 0x0000ff00;
        int tmp = rgb&mask;
        tmp = tmp>>8; //shift over 8 to get the green bits starting at 0
        return tmp;
    }
    public int getRed(){
        int mask = 0x00ff0000;
        int tmp = rgb&mask;
        tmp = tmp>>16;
        return tmp;
    }
    public void setBlue(int b){
        if(b<0||b>255){
            b=0;
        }
        if(debug){System.out.println("b" + b);}
        int mask = 0x00ffff00; //set b in rgb to 0
        rgb = rgb&mask;
        //b does not need to be shifted
        rgb = rgb+b; //add the given value of blue to the value in rgb (set to 0 above)
        if(debug){System.out.println("rgb" + rgb);}
    }
    public void setGreen(int g){
        if(g<0||g>255){
            g=0;
        }
        if(debug){System.out.println("g" + g);}
        int mask = 0x00ff00ff; //set g in rgb to 0
        rgb = rgb&mask;
        g = g<<8; //move green over to the bits where green is stored
        rgb = rgb+g;
        if(debug){System.out.println("rgb" + rgb);}
    }
    public void setRed(int r){
        if(r<0||r>255){
            r=0;
        }
        if(debug){System.out.println("r" + r);}
        int mask = 0x0000ffff; //set r in rgb to 0
        rgb = rgb&mask;
        r = r<<16;
        rgb = rgb+r;
        if(debug){System.out.println("rgb" + rgb);}
    }


    public String printHex(){
        String hex = String.format("%06x", rgb);
        if(debug){System.out.println(hex);}
        hex = "#"+hex;
        if(debug){System.out.println(hex);}
        return hex;
    }
}