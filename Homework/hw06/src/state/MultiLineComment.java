package state;

import analyzer.Analyzer;

public class MultiLineComment extends GroundState {
    private static MultiLineComment state;
    
    private MultiLineComment(){

    }

    /**
     * @return The instance of the class. 
     */
    public static MultiLineComment getInstance(){
        if(state==null){
            state = new MultiLineComment();
        }
        return state;
    }

    @Override
    public void slash(Analyzer a) {
        everythingElse(a);
    }

    @Override
    public void star(Analyzer a) {
        a.changeState(MultiLineExit.getInstance());
    }

    @Override
    public void newline(Analyzer a) {
        //nothing
    }

    @Override
    public void doubleQuote(Analyzer a) {
        everythingElse(a);
    }

    @Override
    public void everythingElse(Analyzer a) {
        a.mlCount++;
    }

    @Override
    public void backSlash(Analyzer a) {
        everythingElse(a);
    }

}