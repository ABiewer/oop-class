package state;

import analyzer.*;

public abstract class GroundState {
    public GroundState(){}
    
    /**
     * If b is an X: do Y
     * " : enter the InString class and increment the code counter
     * / : enter the SingleSlash class
     * everything else: increment the code counter
     * @param c the byte to be analyzed
     * @param a the analyzer
     */
    public void process(char c, Analyzer a){
        switch(c){
            case '/':
                slash(a);
                break;
            case '*':
                star(a);
                break;
            case '\n':
                newline(a);
                break;
            case '"':
                doubleQuote(a);
                break;
            case '\\':
                backSlash(a);
                break;
            case '\r':
                //ignore the windows carriage return
                break;
            default:
                everythingElse(a);
                break;
        }
    }

    public abstract void slash(Analyzer a);
    public abstract void star(Analyzer a);
    public abstract void newline(Analyzer a);
    public abstract void doubleQuote(Analyzer a);
    public abstract void everythingElse(Analyzer a);
    public abstract void backSlash(Analyzer a);

}