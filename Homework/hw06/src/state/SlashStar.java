package state;

import analyzer.Analyzer;

public class SlashStar extends GroundState {
    private static SlashStar state;
    
    private SlashStar(){

    }

    /**
     * @return The instance of the class. 
     */
    public static SlashStar getInstance(){
        if(state==null){
            state = new SlashStar();
        }
        return state;
    }

    @Override
    public void slash(Analyzer a) {
        everythingElse(a);
    }

    @Override
    public void star(Analyzer a) {
        a.jdCount+=3;
        a.changeState(JavaDocComment.getInstance());
    }

    @Override
    public void newline(Analyzer a) {
        everythingElse(a);
    }

    @Override
    public void doubleQuote(Analyzer a) {
        everythingElse(a);
    }

    @Override
    public void everythingElse(Analyzer a) {
        a.mlCount+=2;
        a.changeState(MultiLineComment.getInstance());
    }

    @Override
    public void backSlash(Analyzer a) {
        everythingElse(a);
    }

}