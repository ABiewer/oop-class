package state;

import analyzer.*;

public class InString extends GroundState {
    private static InString state;
    
    private InString(){

    }

    /**
     * @return The instance of the class. 
     */
    public static InString getInstance(){
        if(state==null){
            state = new InString();
        }
        return state;
    }

    @Override
    public void slash(Analyzer a) {
        everythingElse(a);
    }

    @Override
    public void star(Analyzer a) {
        everythingElse(a);
    }

    @Override
    public void newline(Analyzer a) {
        //nothing
    }

    @Override
    public void doubleQuote(Analyzer a) {
        a.codeCount++;
        a.changeState(Code.getInstance());
    }

    @Override
    public void everythingElse(Analyzer a) {
        a.codeCount++;
    }

    @Override
    public void backSlash(Analyzer a) {
        a.codeCount++;
        a.changeState(BashInString.getInstance());
    }
}