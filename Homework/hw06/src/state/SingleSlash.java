package state;

import analyzer.*;

public class SingleSlash extends GroundState{
    private static SingleSlash state;
    
    private SingleSlash(){

    }

    /**
     * @return The instance of the class. 
     */
    public static SingleSlash getInstance(){
        if(state==null){
            state = new SingleSlash();
        }
        return state;
    }

    @Override
    public void slash(Analyzer a) {
        a.slCount+=2;
        a.changeState(SingleLineComment.getInstance());
    }

    @Override
    public void star(Analyzer a) {
        a.changeState(SlashStar.getInstance());
    }

    @Override
    public void newline(Analyzer a) {
        a.codeCount++;
        a.changeState(Code.getInstance());
    }

    @Override
    public void doubleQuote(Analyzer a) {
        everythingElse(a);
    }

    @Override
    public void everythingElse(Analyzer a) {
        a.codeCount+=2;
        a.changeState(Code.getInstance());
    }

    @Override
    public void backSlash(Analyzer a) {
        everythingElse(a);
    }

}