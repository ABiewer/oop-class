package state;

import analyzer.Analyzer;

public class JavaDocExit extends GroundState {
    private static JavaDocExit state;
    
    private JavaDocExit(){

    }

    /**
     * @return The instance of the class. 
     */
    public static JavaDocExit getInstance(){
        if(state==null){
            state = new JavaDocExit();
        }
        return state;
    }

    @Override
    public void slash(Analyzer a) {
        a.jdCount+=2;
        a.changeState(Code.getInstance());
    }

    @Override
    public void star(Analyzer a) {
        a.jdCount++;
    }

    @Override
    public void newline(Analyzer a) {
        a.jdCount--;
        everythingElse(a);
    }

    @Override
    public void doubleQuote(Analyzer a) {
        everythingElse(a);
    }

    @Override
    public void everythingElse(Analyzer a) {
        a.jdCount+=2;
        a.changeState(JavaDocComment.getInstance());
    }

    @Override
    public void backSlash(Analyzer a) {
        everythingElse(a);
    }

}