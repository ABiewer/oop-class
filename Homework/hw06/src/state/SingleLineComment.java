package state;

import analyzer.Analyzer;

public class SingleLineComment extends GroundState {
    private static SingleLineComment state;
    
    private SingleLineComment(){

    }

    /**
     * @return The instance of the class. 
     */
    public static SingleLineComment getInstance(){
        if(state==null){
            state = new SingleLineComment();
        }
        return state;
    }

    @Override
    public void slash(Analyzer a) {
        everythingElse(a);
    }

    @Override
    public void star(Analyzer a) {
        everythingElse(a);
    }

    @Override
    public void newline(Analyzer a) {
        a.changeState(Code.getInstance());
    }

    @Override
    public void doubleQuote(Analyzer a) {
        everythingElse(a);
    }

    @Override
    public void everythingElse(Analyzer a) {
        a.slCount++;
    }

    @Override
    public void backSlash(Analyzer a) {
        everythingElse(a);
    }

}