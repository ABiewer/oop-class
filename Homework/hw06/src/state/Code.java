package state;

import analyzer.*;

public class Code extends GroundState{
    private static Code state;
    
    private Code(){}

    /**
     * @return The instance of the class. 
     */
    public static Code getInstance(){
        if(state==null){
            state = new Code();
        }
        return state;
    }


    @Override
    public void slash(Analyzer a) {
        a.changeState(SingleSlash.getInstance());
    }

    @Override
    public void star(Analyzer a) {
        everythingElse(a);
    }

    @Override
    public void newline(Analyzer a) {
        //nothing
    }

    @Override
    public void doubleQuote(Analyzer a) {
        a.codeCount++;
        a.changeState(InString.getInstance());
    }

    @Override
    public void everythingElse(Analyzer a) {
        a.codeCount++;
    }

    @Override
    public void backSlash(Analyzer a) {
        everythingElse(a);
    }
}