package state;

import analyzer.Analyzer;

public class BashInString extends GroundState {
    private static BashInString state;
    
    private BashInString(){

    }

    /**
     * @return The instance of the class. 
     */
    public static BashInString getInstance(){
        if(state==null){
            state = new BashInString();
        }
        return state;
    }

    @Override
    public void slash(Analyzer a) {
        everythingElse(a);
    }

    @Override
    public void star(Analyzer a) {
        everythingElse(a);
    }

    @Override
    public void newline(Analyzer a) {
        a.changeState(InString.getInstance());
    }

    @Override
    public void doubleQuote(Analyzer a) {
        everythingElse(a);
    }

    @Override
    public void everythingElse(Analyzer a) {
        a.codeCount++;
        a.changeState(InString.getInstance());
    }

    @Override
    public void backSlash(Analyzer a) {
        everythingElse(a);
    }

}