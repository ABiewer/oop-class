package state;

import analyzer.Analyzer;

public class MultiLineExit extends GroundState {
    private static MultiLineExit state;
    
    private MultiLineExit(){

    }

    /**
     * @return The instance of the class. 
     */
    public static MultiLineExit getInstance(){
        if(state==null){
            state = new MultiLineExit();
        }
        return state;
    }

    @Override
    public void slash(Analyzer a) {
        a.mlCount+=2;
        a.changeState(Code.getInstance());
    }

    @Override
    public void star(Analyzer a) {
        a.mlCount++;
    }

    @Override
    public void newline(Analyzer a) {
        a.mlCount--;
        everythingElse(a);
    }

    @Override
    public void doubleQuote(Analyzer a) {
        everythingElse(a);
    }

    @Override
    public void everythingElse(Analyzer a) {
        a.mlCount+=2;
        a.changeState(MultiLineComment.getInstance());
    }

    @Override
    public void backSlash(Analyzer a) {
        everythingElse(a);
    }

}