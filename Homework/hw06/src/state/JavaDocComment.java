package state;

import analyzer.Analyzer;

public class JavaDocComment extends GroundState {
    private static JavaDocComment state;
    
    private JavaDocComment(){

    }

    /**
     * @return The instance of the class. 
     */
    public static JavaDocComment getInstance(){
        if(state==null){
            state = new JavaDocComment();
        }
        return state;
    }

    @Override
    public void slash(Analyzer a) {
        everythingElse(a);
    }

    @Override
    public void star(Analyzer a) {
        a.changeState(JavaDocExit.getInstance());
    }

    @Override
    public void newline(Analyzer a) {
        //nothing
    }

    @Override
    public void doubleQuote(Analyzer a) {
        everythingElse(a);
    }

    @Override
    public void everythingElse(Analyzer a) {
        a.jdCount++;
    }

    @Override
    public void backSlash(Analyzer a) {
        everythingElse(a);
    }

}