package analyzer;

import java.io.File;
import java.io.IOException;
import java.util.Scanner;

import state.*;

public class Analyzer {
    private GroundState state;
    public int jdCount;
    public int slCount;
    public int mlCount;
    public int codeCount;

    /**
     * Constructs the necessary components and analyzes the file given. 
     * @param filepath The path to the file.
     * @throws IOException When the path to the file is an invalid file. 
     */
    public Analyzer(String filepath) throws IOException {
        jdCount = slCount = mlCount = codeCount = 0;

        state = Code.getInstance();

        File inputFile = new File(filepath);

        Scanner s = new Scanner(inputFile);

        while(s.hasNextLine()){
            String nextline = s.nextLine();
            for(int i = 0;i<nextline.length();i++){
                char c = nextline.charAt(i);
                state.process(c, this);
            }
            state.process('\n', this);
        }

        s.close();
    }

    /**
     * @return A Counter object with the relevant counters. 
     */
    public Counter analyze(){
        return new Counter(slCount,mlCount,jdCount,codeCount);
    }

    /**
     * @param state The new state the analyzer should take on. 
     */
    public void changeState(GroundState state){
        this.state = state;
    }
}