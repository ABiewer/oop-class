package driver;

import java.io.File;
import java.io.IOException;

import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JTextPane;

import analyzer.*;

public class Driver {
    public static void main(String[] args){
        try{
            File f = null;
            JFileChooser jfc = new JFileChooser();

            int returnValue = jfc.showOpenDialog(null);
            if (returnValue == JFileChooser.APPROVE_OPTION)
                f = jfc.getSelectedFile();
            else{
                System.out.println("Invalid File");
                return;
            }

            Analyzer a = new Analyzer(f.getAbsolutePath());
            Counter c = a.analyze();

            JFrame output = new JFrame();
            JTextPane panel = new JTextPane();

            String s = "Code: " + c.codeCount;
            s += "Single Line: " + c.slCount;
            s += "Multi Line: " + c.mlCount;
            s += "JavaDoc: " + c.jdCount;

            panel.setText(s);
            output.add(panel);
            output.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
            output.setSize(500,500);
            output.setVisible(true);

            System.out.println("Code: " + c.codeCount);
            System.out.println("\nSingle Line: " + c.slCount);
            System.out.println("\nMulti Line: " + c.mlCount);
            System.out.println("\nJavaDoc: " + c.jdCount);
        }
        catch(IOException e){
            e.printStackTrace();
        }
    }
}