package app;
import javax.swing.*;

public class App {
    public static void main(String[] args) throws Exception {
        JFrame f= new JFrame();
        int hoursPerCheck = 500;
        int carn = 0;
        int herb = 0;
        int scav = 0;
        int creatureID = 0;
        int starthour = 0;
        int endhour = 0;
        String input = JOptionPane.showInputDialog(f,"Enter the number of carnivores.");
        try{
            carn = Integer.parseInt(input);
        }
        catch(NumberFormatException e){
            System.out.println("Invalid input for carnivores.");
        }
        input = JOptionPane.showInputDialog(f, "Enter the number of herbivores.");
        try{
            herb = Integer.parseInt(input);
        }
        catch(NumberFormatException e){
            System.out.println("Invalid input for herbivores.");
        }
        input = JOptionPane.showInputDialog(f, "Enter the number of scavengers.");
        try{
            scav = Integer.parseInt(input);
        }
        catch(NumberFormatException e){
            System.out.println("Invalid input for scavengers.");
        }
        input = JOptionPane.showInputDialog(f, "Enter the number of hours per print.");
        try{
            hoursPerCheck = Integer.parseInt(input);
        }
        catch(NumberFormatException e){
            System.out.println("Invalid input for the number of hours.");
        }
        System.out.println("Starting");
        int xsize = 5;
        int ysize = 5;
        Terrain myMap = new Terrain(xsize,ysize,carn,herb,scav);
        System.out.println(myMap.toString());
        while(myMap.getLiving()){
            myMap.incTime();
            if(myMap.getTime()%hoursPerCheck==0){
                System.out.println("Time: " + myMap.getTime() + " Alive: " + myMap.getLivingNumber());
            }
        }
        System.out.println("All of the creatures are dead now.");
        while(creatureID!=-1){
            input = JOptionPane.showInputDialog(f,"Enter the ID of the creature you want to see information about: (-1 to quit)");
            try{
                creatureID = Integer.parseInt(input);
            }
            catch(NumberFormatException e){
                System.out.println("Invalid ID input");
                continue;
            }
            if(creatureID<0||creatureID>myMap.getNumberCreatures()){
                System.out.println("Invalid ID input");
                continue;
            }
            input = JOptionPane.showInputDialog(f,"Enter the starting hour: ");
            try{
                starthour = Integer.parseInt(input);
            }
            catch(NumberFormatException e){
                System.out.println("Invalid start hour input");
                continue;
            }
            input = JOptionPane.showInputDialog(f,"Enter the ending hour: ");
            try{
                endhour = Integer.parseInt(input);
            }
            catch(NumberFormatException e){
                System.out.println("Invalid ending hour input");
                continue;
            }
            if(endhour<starthour||endhour<0){
                System.out.println("Invalid hours");
                continue;
            }
            myMap.getHistory(creatureID,starthour,endhour);
        }
    }
}