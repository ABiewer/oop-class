package app;

public class Herbivore extends Creature{
    Herbivore(){
        super();
        deathAge = random.nextInt()%(5*365*24)+(10*365*24); //The creature lives between 10 and 15 years.
        type = 'h'; 
    }

    @Override
    protected void tryToEat() {
        addHistoryEvent("Trying to Eat", "Hunger");
        if(here.getType()=='G'){
            hungerbar = 1.0;
        }
        else{
            move();
        }
    }

    @Override
    protected void move() {
        hungerbar-=.01;
        if(here.surroundingCellSize()>0){
            here.removeCreature(this, here.getSurroundingCell(random.nextInt(10)%(here.surroundingCellSize())));
        }
    }

    @Override
    protected void flee() {
        move();
    }
}