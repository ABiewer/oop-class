package app;
import java.util.ArrayList;

public class TerrainCell{
    private char type;
    private ArrayList<Creature> presentCreatures;
    private ArrayList<TerrainCell> surroundingCells;
    private Terrain map;
    private int x;
    private int y;

    TerrainCell(Terrain map, int x, int y){
        type = 'n';
        presentCreatures = new ArrayList<Creature>();
        surroundingCells = new ArrayList<TerrainCell>();
        this.map = map;
        this.x = x;
        this.y = y;
    }

    public void addCreature(Creature addee){
        addee.setTerrain(this);
        presentCreatures.add(addee);
        addee.checkWater();
    }

    public void removeCreature(Creature removee, TerrainCell destination){
        presentCreatures.remove(removee);
        destination.addCreature(removee);
        removee.setTerrain(destination);
    }

    public int getCount(){
        return presentCreatures.size();
    }
    public int getLiving(){
        int retval = 0;
        for(Creature temp : presentCreatures){
            if(temp.isAlive()){
                retval++;
            }
        }
        return retval;
    }
    public int getDead(){
        int retval = 0;
        for(Creature temp : presentCreatures){
            if(!temp.isAlive()){
                retval++;
            }
        }
        return retval;
    }

    public char getType(){
        return type;
    }

    public void setType(char type){
        if(type!='W'&&type!='G'&&type!='M'&&type!='D'){
            throw new IllegalArgumentException("invalid set for terraintype");
        }
        else{
            this.type = type;
        }
    }

    public void incrementTime(){
        for(int i = 0;i<presentCreatures.size();i++){
            presentCreatures.get(i).increment();
        }
    }

    public void addSurroundingCell(TerrainCell addee){
        surroundingCells.add(addee);
    }

    public int surroundingCellSize(){
        return surroundingCells.size();
    }

    public TerrainCell getSurroundingCell(int i){
        if(i<0||i>surroundingCellSize()){
            throw new IllegalArgumentException("Incorrect array index");
        }
        return surroundingCells.get(i);
    }

    public Creature getOneLiving(Creature hunter){
        Creature retVal = null;
        if(getLiving()>0){
            for(int i= 0;i<presentCreatures.size();i++){
                if(presentCreatures.get(i).isAlive() && !presentCreatures.get(i).equals(hunter)){
                    retVal = presentCreatures.get(i);
                    break;
                }
            }
        }
        return retVal;
    }

    public Creature getOneDead(){
        Creature retVal=null;
        if(getDead()>0){
            for(int i = 0;i<presentCreatures.size();i++){
                Creature target = presentCreatures.get(i);
                if(!target.isAlive() && target.getScavengerMealsLeft()>0){
                    retVal = target;
                    break;
                }
            }
        }
        return retVal;
    }

    public void fleeAll(Creature hunter){
        for(int i = 0;i<presentCreatures.size();i++){
            Creature fleer = presentCreatures.get(i);
            if(fleer.isAlive() && !fleer.equals(hunter)){
                fleer.flee();
            }
        }
    }

    public int getTime(){
        return map.getTime();
    }

    public int getX(){
        return x;
    }
    public int getY(){
        return y;
    }
}