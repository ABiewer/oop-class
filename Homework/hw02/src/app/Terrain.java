package app;
import java.util.ArrayList;
import java.util.Random;

public class Terrain{
    private ArrayList<ArrayList<TerrainCell>> map;
    private int xsize;
    private int ysize;
    private int time;
    private int scav;
    private ArrayList<Creature> creatures;
    Random random;
    private boolean scavengersAdded;

    Terrain(int col, int row, int carn, int herb, int scav){
        map = new ArrayList<ArrayList<TerrainCell>>();
        this.scav = scav;
        scavengersAdded = false;
        random = new Random();
        time = 0;
        HistoryEvent.setTerrain(this);
        creatures = new ArrayList<Creature>();
        if(col<1){
            col = 1;
        }
        if(row<1){
            row = 1;
        }
        for(int i = 0;i<row;i++){
            ArrayList<TerrainCell> tmp = new ArrayList<TerrainCell>();
            for(int j = 0;j<col;j++){
                tmp.add(new TerrainCell(this, i, j));
            }
            map.add(tmp);
        }
        this.xsize = row;
        this.ysize = col;
        TerrainConstructorHelper();
        TerrainCellSurroundingAdder();
        addCarnHerb(carn, herb);
    }

    private void TerrainConstructorHelper(){
        int typenum = 0;
        int blocksize = 0;
        if(xsize < 4 || ysize < 4){ //the block sizes for creating terrain must be less than half the maximum distance between the sides
                                    //if the distance is 3 or less, only blocks of size 1 can be made. Avoid calls to random
            for(int i = 0;i<xsize;i++){
                for(int j = 0;j<ysize;j++){
                    typenum = random.nextInt(4)+1;
                    switch(typenum){
                        case 4:
                            map.get(i).get(j).setType('W');
                            break;
                        case 1:
                            map.get(i).get(j).setType('M');
                            break;
                        case 2:
                            map.get(i).get(j).setType('G');
                            break;
                        case 3:
                            map.get(i).get(j).setType('D');
                            break;
                    }
                }
            }

        }
        else{
            int xpos = 0;
            int ypos = 0;
            int maxsize;
            //the maximum size for blocks are half the width of the smallest dimension
            if(xsize>ysize){
                maxsize = ysize/2;
            }
            else{
                maxsize = xsize/2;
            }
            while(true){
                //System.out.println("xpos: " + xpos + "\nypos: " + ypos);
                //start at x=0 y=0, move left creating random blocks until hit the edge, move down 1 column, search for terraincells with type 'n'
                //and create blocks from those, not replacing already placed blocks. 
                if(map.get(xpos).get(ypos).getType()=='n'){
                    blocksize = random.nextInt(maxsize)+1;
                    //System.out.println("blocksize: " + blocksize);
                    typenum = random.nextInt(4)+1;
                    if(xpos+blocksize>xsize){ 
                        //there are more cells that will be set than are left, so ignore them
                        for(int i = xpos;i<xsize;i++){ 
                            //loop from the position to the end of the row
                            if(ypos+blocksize>ysize){
                                //likewise, if there are not enough spots, they will be ignored.
                                //System.out.println("X and Y too large");
                                for(int j = ypos;j<ysize;j++){
                                    //only loop over the spots that aren't filled
                                    switch(typenum){
                                        case 4:
                                            map.get(i).get(j).setType('W');
                                            break;
                                        case 1:
                                            map.get(i).get(j).setType('M');
                                            break;
                                        case 2:
                                            map.get(i).get(j).setType('G');
                                            break;
                                        case 3:
                                            map.get(i).get(j).setType('D');
                                            break;
                                    }
                                }
                            }
                            else{
                                //ypos + blocksize is less than ysize
                                //System.out.println("X too large");
                                for(int j = ypos;j<ypos+blocksize;j++){
                                    //loop over the blocksize
                                    switch(typenum){
                                        case 4:
                                            map.get(i).get(j).setType('W');
                                            break;
                                        case 1:
                                            map.get(i).get(j).setType('M');
                                            break;
                                        case 2:
                                            map.get(i).get(j).setType('G');
                                            break;
                                        case 3:
                                            map.get(i).get(j).setType('D');
                                            break;
                                    }
                                }
                            }
                        }
                    }

                    else{
                        //xpos + blocksize is less than xsize
                        for(int i = xpos;i<xpos+blocksize;i++){
                            //loop over the valid blocks
                            if(ypos+blocksize>ysize){
                                //likewise, if there are not enough spots, they will be ignored.
                                //System.out.println("Y too large");
                                for(int j = ypos;j<ysize;j++){
                                    //only loop over the spots that aren't filled
                                    switch(typenum){
                                        case 4:
                                            map.get(i).get(j).setType('W');
                                            break;
                                        case 1:
                                            map.get(i).get(j).setType('M');
                                            break;
                                        case 2:
                                            map.get(i).get(j).setType('G');
                                            break;
                                        case 3:
                                            map.get(i).get(j).setType('D');
                                            break;
                                    }
                                }
                            }
                            else{
                                //ypos + blocksize is less than ysize
                                //System.out.println("Neither too large");
                                for(int j = ypos;j<ypos+blocksize;j++){
                                    //loop over the blocksize
                                    switch(typenum){
                                        case 4:
                                            map.get(i).get(j).setType('W');
                                            break;
                                        case 1:
                                            map.get(i).get(j).setType('M');
                                            break;
                                        case 2:
                                            map.get(i).get(j).setType('G');
                                            break;
                                        case 3:
                                            map.get(i).get(j).setType('D');
                                            break;
                                    }
                                }
                            }
                        }
                    }
                }
                xpos++;
                if(xpos >= xsize){
                    //xpos == xsize means the position is at the end. 
                    ypos++;
                    if(ypos >= ysize){
                        //ypos == ysize means the end of the array has been reached. 
                        break;
                    }
                    xpos = 0;
                }
            }
        }
    }

    private void TerrainCellSurroundingAdder(){
        //this function adds the surrounding cells of all cells so long as they are not water. 
        for(int i = 0;i<xsize;i++){
            for(int j=0;j<ysize;j++){
                //for every terraincell
                TerrainCell temp = map.get(i).get(j);
                for(int k = 0;k<3;k++)
                    for(int l = 0;l<3;l++){
                        //and for each surrounding cell
                        int xpos = k+i-1;
                        int ypos = l+j-1;
                        //if the positions are valid positions, 
                        if(xpos>=0&&xpos<xsize)
                            if(ypos>=0&&ypos<ysize)
                                if(!(k==1&&l==1))
                                    if(map.get(xpos).get(ypos).getType() != 'W')
                                        temp.addSurroundingCell(map.get(xpos).get(ypos));
                                        //and are not water, then add them to the list
                    }
                //add each of the surrounding cells excluding the current cell
            }
        }
    }

    public void incTime(){
        time++;
        for(int i = 0;i<xsize;i++){
            for(int j = 0;j<ysize;j++){
                map.get(i).get(j).incrementTime();
            }
        }
        if(!scavengersAdded&&time>5000){
            addScavengers(scav);
            scavengersAdded = true;
        }
    }
    
    public String toString(){
        StringBuilder sb = new StringBuilder();
        for(int i = 0;i<xsize;i++){
            for(int j = 0;j<ysize;j++){
                sb.append(map.get(i).get(j).getType());
                sb.append(' ');
            }
            sb.append("\n");
        }
        sb.append("\n");
        return sb.toString();
    }

    private void addCarnHerb(int carn, int herb){
        for(int i = 0;i<carn;i++){ //carn>=0 checked by for loop
            Carnivore carnie = new Carnivore();
            map.get(random.nextInt(xsize)).get(random.nextInt(ysize)).addCreature(carnie);
            creatures.add(carnie);
        }
        for(int i = 0;i<herb;i++){ //herb>=0 checked by for loop
            Herbivore herbie = new Herbivore();
            map.get(random.nextInt(xsize)).get(random.nextInt(ysize)).addCreature(herbie);
            creatures.add(herbie);
        }
    }

    private void addScavengers(int scav){
        for(int i = 0;i<scav;i++){ //scav>=0 checked by for loop
            Scavenger scavie = new Scavenger();
            map.get(random.nextInt(xsize)).get(random.nextInt(ysize)).addCreature(scavie);
            creatures.add(scavie);
        }
    }

    public int getTime(){
        return time;
    }

    public boolean getLiving(){
        for(int i = 0;i<creatures.size();i++){
            if(creatures.get(i).isAlive()){
                return true;
            }
        }
        return false;
    }

    public int getLivingNumber(){
        int counter = 0;
        for(int i = 0;i<creatures.size();i++){
            if(creatures.get(i).isAlive()){
                counter++;
            }
        }
        return counter;
    }

    public int getNumberCreatures(){
        return creatures.size();
    }

    public int xsize(){
        return xsize;
    }
    public int ysize(){
        return ysize;
    }

    public void getHistory(int creatureID,int starthour,int endhour){
        Creature target;
        for(int i = 0;i<creatures.size();i++){
            target = creatures.get(i);
            if(target.getID()==creatureID){
                target.getHistory(starthour,endhour);
            }
        }
    }
}