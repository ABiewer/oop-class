package app;

public class Carnivore extends Creature{
    Carnivore(){
        super();
        deathAge = random.nextInt()%(2*365*24)+(5*365*24); //The creature lives between 5 and 7 years. 
        type = 'c';
    }

    @Override
    protected void tryToEat() {
        addHistoryEvent("Trying to Eat", "Hunger");
        int numLive = here.getLiving();
        if(numLive>1){
            Creature target = here.getOneLiving(this);
            if(target.getType()=='c'){
                if(random.nextInt()%10>=7){ //70% chance of survival
                    target.kill();
                    target.addHistoryEvent("Died to Predator", "Death");
                    hungerbar = 1.0;
                }
                else{
                    target.addHistoryEvent("Was Attacked by Predator", "Attack");
                    here.fleeAll(this);
                }
            }
            else if(target.getType()=='h'){
                if(random.nextInt()%10>=6){ //60% chance of survival
                    target.kill();
                    target.addHistoryEvent("Died to Predator", "Death");
                    hungerbar = 1.0;
                }
                else{
                    target.addHistoryEvent("Was Attacked by Predator", "Attack");
                    here.fleeAll(this);
                }
            }
            else if(target.getType()=='s'){
                if(random.nextInt()%10>=5){ //50% chance of survival
                    target.kill();
                    target.addHistoryEvent("Died to Predator", "Death");
                    hungerbar = 1.0;
                }
                else{
                    target.addHistoryEvent("Was Attacked by Predator", "Attack");
                    here.fleeAll(this);
                }
            }
        }
        else{
            move();
        }
    }

    @Override
    protected void move() {
        hungerbar-=.01;
        if(here.surroundingCellSize()>0){
            here.removeCreature(this, here.getSurroundingCell(random.nextInt(10)%(here.surroundingCellSize())));
        }      
    }

    @Override
    protected void flee() {
        addHistoryEvent("Fleeing from hostile creature", "Flee");
        move();
    }
}