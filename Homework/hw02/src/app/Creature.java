package app;
import java.util.Random;
import java.util.ArrayList;

public abstract class Creature{
    private static int IDcounter = 0;
    protected int ID;
    protected Random random;
    protected double hungerbar;
    protected int age;
    protected int deathAge;
    protected boolean status; //true = alive, false = dead
    protected char type;
    protected TerrainCell here;
    protected ArrayList<HistoryEvent> history;
    protected int scavengerMealsLeft;

    Creature(){
        ID = IDcounter;
        IDcounter++;
        hungerbar = 1.0;
        age = 0;
        status = true;
        history = new ArrayList<HistoryEvent>();
        random = new Random();
        scavengerMealsLeft = 2;
    }

    public boolean isAlive(){
        return status;
    }

    private void checkOldAge(){
        if(age>deathAge){
            addHistoryEvent("Died of old age", "Death");
            status = false;
        }
    }

    private void checkStarvation(){
        if(hungerbar<=0){
            addHistoryEvent("Died to Starvation", "Death");
            status = false;
        }
    }

    public void increment(){
        if(!isAlive()){
            return;
        }
        if(hungerbar <= .4){
            tryToEat();
        }
        hungerbar -= .01;
        checkStarvation();
        age++;
        checkOldAge();
    }

    protected abstract void tryToEat();
    protected abstract void move();
    protected abstract void flee();


    public void setTerrain(TerrainCell here){
        this.here = here;
    }

    public TerrainCell getTerrain(){
        return here;
    }

    public void checkWater(){
        if(here.getType()=='W'){
            addHistoryEvent("Drowned", "Drowned");
            status = false;
        }
    }

    public int getScavengerMealsLeft(){
        return scavengerMealsLeft;
    }

    public void decrementScavengerMeal(){
        scavengerMealsLeft--;
    }

    public char getType(){
        return type;
    }

    public void kill(){
        status = false;
    }

    public void addHistoryEvent(String reason, String type){
        int time = here.getTime();
        int xpos = here.getX();
        int ypos = here.getY();
        history.add(new HistoryEvent(time,reason,type,xpos,ypos));
    }

    public int getID(){
        return ID;
    }

    public void getHistory(int starthour, int endhour){
        System.out.println("The history of animal " + ID + " from " + starthour + " to " + endhour + ".");
        for(int i = 0;i<history.size();i++){
            int currenthour = history.get(i).getHour();
            if(currenthour>starthour&&currenthour<endhour){
                System.out.println(history.get(i).toString());
            }
        }
        System.out.println("Thus ends this animal's history.");
    }
}