package app;

public class Scavenger extends Creature{
    Scavenger(){
        super();
        deathAge = random.nextInt()%(2*365*24)+(8*365*24); //The creature lives between 8 and 10 years. 
        type = 's';
    }

    @Override
    protected void tryToEat() {
        addHistoryEvent("Trying to Eat", "Hunger");
        if(here.getDead()>0){
            Creature target = here.getOneDead();
            if(target!=null){
                target.decrementScavengerMeal();
                target.addHistoryEvent("Being Scavanged", "Scavanged");
                hungerbar = 1.0;
            }
            else{
                addHistoryEvent("Hunting for food", "Move");
                move();
            }
        }
        else{
            addHistoryEvent("Hunting for food", "Move");
            move();
        }
    }

    @Override
    protected void move(){
        hungerbar-=.01;
        if(here.surroundingCellSize()>0){
            here.removeCreature(this, here.getSurroundingCell(random.nextInt(10)%(here.surroundingCellSize())));
        }
    }

    @Override
    protected void flee() {
        addHistoryEvent("Hostile Creature", "Flee");
        move();
    }
}