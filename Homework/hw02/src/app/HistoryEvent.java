package app;

public class HistoryEvent{
    private int hour;
    private String reason;
    private String type;
    private static Terrain map;
    private int xpos;
    private int ypos;


    public static void setTerrain(Terrain mp){
        map = mp;
    }

    HistoryEvent(int hour, String reason, String type, int xpos, int ypos){
        this.hour = hour;
        this.reason = reason;
        this.type = type;
        this.xpos = xpos;
        this.ypos = ypos;
    }

    public int getHour(){
        return hour;
    }

    public String toString(){
        StringBuilder sb = new StringBuilder();
        sb.append("Hour: " + hour + "\n");
        sb.append("History Event: " + type + "\n");
        sb.append("Reason of Event: " + reason + "\n");
        sb.append("\nMap: " + "\n");
        sb.append(map.toString() + "\n");
        for(int y = 0;y<map.ysize();y++){
            if(ypos == y){
                int x = 0;
                for(;x<xpos;x++){
                    sb.append("- ");
                }
                sb.append("X ");
                x++;
                for(;x<map.xsize();x++){
                    sb.append("- ");
                }
                sb.append("\n");
            }
            else{
                for(int x = 0;x<map.xsize();x++){
                    sb.append("- ");
                }
                sb.append("\n");
            }
        }
        return sb.toString();
    }
}