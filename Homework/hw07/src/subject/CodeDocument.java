package subject;

import interfaces.*;

public class CodeDocument implements CodeDocumentInterface {
    private Subject subject;
    private String workingCode;

    public CodeDocument() {
        subject = new SubjectImpl();
        workingCode = "";
    }

    @Override
    public void attach(Observer o) {
        subject.attach(o);
    }

    public void detach(String ipAddress, int port){
        ((SubjectImpl)subject).detach(ipAddress, port);
    }

    @Override
    public void detach(Observer o) {
        subject.detach(o);
    }

    @Override
    public void notifyObservers() {
        subject.notifyObservers();
    }

    @Override
    public String getText(){
        return workingCode;
    }

    @Override
    public void setText(String text){
        workingCode = text;
    }
    
}