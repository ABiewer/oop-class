package subject;

import interfaces.*;
import interfaces.Observer;
import proxies.GUIProxy;

import java.util.*;

public class SubjectImpl implements Subject {

    private List<Observer> observers;

    public SubjectImpl(){
        observers = new ArrayList<Observer>();
    }

    @Override
    public void attach(Observer o) {
        observers.add(o);
    }

    public void detach(String ipAddress, int port){
        for(Observer o : observers){
            GUIProxy gp = (GUIProxy)o;
            if(gp.ip == ipAddress && gp.port == port){
                detach(gp);
                return;
            }
        }
    }

    @Override
    public void detach(Observer o) {
        observers.remove(o);
    }

    @Override
    public void notifyObservers() {
        for(Observer o : observers){
            o.update();
        }
    }
    
}