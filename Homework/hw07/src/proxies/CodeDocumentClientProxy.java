package proxies;

import java.io.*;
import java.net.*;

import interfaces.CodeDocumentInterface;
import interfaces.Observer;
import observer.CodeGUI;

public class CodeDocumentClientProxy implements CodeDocumentInterface {
    private CodeGUI cg;
    private String text;

    public CodeDocumentClientProxy() throws IOException {
        cg = new CodeGUI(this);
        /*
        //needs to listen for GUI proxy updates
        ServerSocket welcomeSocket = new ServerSocket(4000);
        Thread t = new Thread(new Runnable(){
        
            @Override
            public void run() {
                while(true){
                    try{
                        Socket mySocket = welcomeSocket.accept();

                        String clientIP = mySocket.getInetAddress().getHostAddress();
                        System.out.println("Request entered by : " + clientIP);

                        DataOutputStream outToClient = new DataOutputStream(mySocket.getOutputStream());
                        BufferedReader inFromClient = new BufferedReader(new InputStreamReader(mySocket.getInputStream()));

                        //read the request from the client
                        //block until a request is made
                        String requestFromClient = inFromClient.readLine();

                        //print out the request to the screen
                        System.out.println("Client Request: " + requestFromClient);
                        
                        if(requestFromClient.startsWith("setText:")){
                            setText(requestFromClient.substring(8));
                            outToClient.writeBytes("setText success!");
                        }
                        else{
                            outToClient.writeBytes("Invalid Request!\n");
                        }
                    }
                    catch(IOException e){
                        System.out.println("Error with CodeDocClientProxy");
                        e.printStackTrace();
                    }
                }
            }
        });*/
    }

    @Override
    public void attach(Observer o) {
        //nothing attaches to it
    }

    @Override
    public void detach(Observer o) {
        //nothing detaches from it
    }

    @Override
    public void notifyObservers() {
        //this has no observers
    }

    @Override
    public String getText() {
        return text;
    }

    @Override
    public void setText(String text) {
        this.text = text;
        cg.update();
    }
}