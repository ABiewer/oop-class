package proxies;

import java.net.*;
import java.io.*;

import interfaces.CodeDocumentInterface;
import interfaces.Observer;

public class CodeDocumentProxy implements CodeDocumentInterface {
    private String ipAddress;
    private int port;
    private BufferedReader inFromKeyboard;

    public CodeDocumentProxy() throws IOException {
        inFromKeyboard = new BufferedReader(new InputStreamReader(System.in));

        System.out.println("Welcome to the TCP Client!");
        System.out.println("Enter the IP address of the machine to connect to: ");
        ipAddress = inFromKeyboard.readLine();

        System.out.println("Enter the port number of the application to connect to: ");
        String PortToConnectTo = inFromKeyboard.readLine();
        port = Integer.parseInt(PortToConnectTo);
    }

    @Override
    public void attach(Observer o) {
        try{
            Socket clientSocket = new Socket(ipAddress, port);
            DataOutputStream outToServer = new DataOutputStream(clientSocket.getOutputStream());
            BufferedReader inFromServer = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));

            String request = "attach:"+"55555";
            outToServer.writeBytes(request + "\n");

            String responseFromServer = inFromServer.readLine();

            System.out.println("Server response: " + responseFromServer);

            //close the socket after we are done with it
            clientSocket.close();
        }
        catch(IOException e){
            System.out.println("Could not connect to server.");
            e.printStackTrace();
        }
    }

    @Override
    public void detach(Observer o) {
        try{
            Socket clientSocket = new Socket(ipAddress, port);
            DataOutputStream outToServer = new DataOutputStream(clientSocket.getOutputStream());
            BufferedReader inFromServer = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));

            String request = "detach:"+"55555";
            outToServer.writeBytes(request + "\n");

            String responseFromServer = inFromServer.readLine();

            System.out.println("Server response: " + responseFromServer);

            //close the socket after we are done with it
            clientSocket.close();
        }
        catch(IOException e){
            System.out.println("Could not connect to server.");
            e.printStackTrace();
        }
    }

    @Override
    public void notifyObservers() {
        //this doesn't get called from here. 
    }

    @Override
    public String getText() {
        //nothing calls getText from this class
        return null;
    }

    @Override
    public void setText(String text) {
        try{
            Socket clientSocket = new Socket(ipAddress, port);
            DataOutputStream outToServer = new DataOutputStream(clientSocket.getOutputStream());
            BufferedReader inFromServer = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));

            String request = "setText:"+text;
            outToServer.writeBytes(request + "\n");

            String responseFromServer = inFromServer.readLine();

            System.out.println("Server response: " + responseFromServer);

            //close the socket after we are done with it
            clientSocket.close();
        }
        catch(IOException e){
            System.out.println("Could not connect to server.");
            e.printStackTrace();
        }
    }

}