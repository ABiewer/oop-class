package proxies;

import interfaces.CodeDocumentInterface;
import interfaces.Observer;
import subject.CodeDocument;

import java.net.*;
import java.io.*;

public class GUIClientProxy implements Observer, Runnable{
    CodeDocumentInterface document;
    public GUIClientProxy() throws IOException {
        document = new CodeDocument();

        printIPInfo();

        ServerSocket welcomeSocket = new ServerSocket(55555);
        
        while(true){
            Socket mySocket = welcomeSocket.accept();

            String clientIP = mySocket.getInetAddress().getHostAddress();
            System.out.println("Request entered by : " + clientIP);

            DataOutputStream outToClient = new DataOutputStream(mySocket.getOutputStream());
            BufferedReader inFromClient = new BufferedReader(new InputStreamReader(mySocket.getInputStream()));

            //read the request from the client
            //block until a request is made
            String requestFromClient = inFromClient.readLine();

            //print out the request to the screen
            System.out.println("Client Request: " + requestFromClient);

            //enforce a protocol
            if(requestFromClient.startsWith("attach"))
            {
                int port = Integer.parseInt(requestFromClient.substring(7));
                document.attach(new GUIProxy(clientIP, port, document));
                outToClient.writeBytes("Attach success!");
            }
            else if(requestFromClient.startsWith("detach"))
            {
                int port = Integer.parseInt(requestFromClient.substring(7));
                ((CodeDocument)document).detach(clientIP, port);
                outToClient.writeBytes("Detach success!");
            }
            else if(requestFromClient.startsWith("setText"))
            {
                document.setText(requestFromClient.substring(8));
                document.notifyObservers();
                outToClient.writeBytes("setText success!");
            }
            else
            {
                outToClient.writeBytes("Invalid Request!\n");
            }

            //close the socket
            mySocket.close();
        }
    }

    @Override
    public void update() {

    }

    @Override
    public void run() {

    }

    public void printIPInfo() throws UnknownHostException {
        System.out.println("Welcome to the TCP Server!");

        //find out the IP address and host name of this machine
        //for more info go to api and search for the class called 
        //InetAddress
        String myIP = InetAddress.getLocalHost().getHostAddress();
        System.out.println("My IP address is " + myIP);

        String myHostName = InetAddress.getLocalHost().getHostName();
        System.out.println("My host name is " + myHostName);
    }
    
}