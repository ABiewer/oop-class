package observer;

import java.io.IOException;

import javax.swing.JFrame;
import javax.swing.JTextArea;

import interfaces.CodeDocumentInterface;
import interfaces.Observer;
import proxies.CodeDocumentProxy;

public class CodeGUI extends JFrame implements Observer {
    CodeDocumentInterface document;
    CodeDocumentInterface proxyDoc;
    JTextArea textArea;
    boolean settingText;

    public CodeGUI(CodeDocumentInterface document) throws IOException {
        settingText = false;
        this.document = document;
        proxyDoc = new CodeDocumentProxy();
        proxyDoc.attach(this);

        textArea = new JTextArea();
        textArea.setText(document.getText());
        textArea.getDocument().addDocumentListener(new CodeDocumentListener(this));
        this.add(textArea);

        this.setSize(500, 500);
        this.setLocation(100, 100);
        this.setVisible(true);
    }

    @Override
    public void update() {
        if(!settingText){
            settingText = true;
            textArea.setText(document.getText());
            try{
            Thread.sleep(10);
            }
            catch(InterruptedException e){
                System.out.println("Unable to update");
                e.printStackTrace();
            }
            settingText = false;
        }
    }

    
}