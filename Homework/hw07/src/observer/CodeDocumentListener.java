package observer;

import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

public class CodeDocumentListener implements DocumentListener {
    private CodeGUI cg;
    
    CodeDocumentListener(CodeGUI cg){
        this.cg = cg;
    }

    @Override
    public void changedUpdate(DocumentEvent arg0) {
        //aparently doesn't happen with basic text
    }

    @Override
    public void insertUpdate(DocumentEvent arg0) {
        if(!cg.settingText){
            updateText();
        }
    }

    @Override
    public void removeUpdate(DocumentEvent arg0) {
        if(!cg.settingText){
            updateText();
        }
    }

    private void updateText(){
        cg.settingText = true;
        cg.document.setText(cg.textArea.getText());
        cg.proxyDoc.setText(cg.textArea.getText());
        cg.proxyDoc.notifyObservers();
        cg.settingText = false;
    }
    
}