package subject;

import observer.Observer;

public class CodeDocument implements CodeDocumentInterface {
    private Subject subject;
    private String workingCode;

    public CodeDocument() {
        subject = new SubjectImpl();
        workingCode = "";
    }

    @Override
    public void attach(Observer o) {
        subject.attach(o);
    }

    @Override
    public void detach(Observer o) {
        subject.detach(o);
    }

    @Override
    public void notifyObservers() {
        subject.notifyObservers();
    }

    @Override
    public String getText(){
        return workingCode;
    }

    @Override
    public void setText(String text){
        workingCode = text;
    }
    
}