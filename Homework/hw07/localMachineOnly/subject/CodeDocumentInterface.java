package subject;



public interface CodeDocumentInterface extends Subject{
    String getText();
    void setText(String text);
}