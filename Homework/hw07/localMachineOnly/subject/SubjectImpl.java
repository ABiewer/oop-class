package subject;

import observer.Observer;
import java.util.*;

public class SubjectImpl implements Subject {

    private List<Observer> observers;

    public SubjectImpl(){
        observers = new ArrayList<Observer>();
    }

    @Override
    public void attach(Observer o) {
        observers.add(o);
    }

    @Override
    public void detach(Observer o) {
        observers.remove(o);
    }

    @Override
    public void notifyObservers() {
        for(Observer o : observers){
            o.update();
        }
    }
    
}