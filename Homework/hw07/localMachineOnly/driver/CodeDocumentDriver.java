package driver;

import observer.CodeGUI;
import subject.CodeDocument;

public class CodeDocumentDriver {
    public static void main(String[] args) throws Exception {
        CodeDocument document = new CodeDocument();
        CodeGUI cg = new CodeGUI(document);
        CodeGUI cg2 = new CodeGUI(document);
    }
}