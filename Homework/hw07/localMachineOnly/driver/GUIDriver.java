package driver;

import observer.CodeGUI;
import subject.CodeDocument;

import java.net.*;
import java.io.*;
import java.util.*;

import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;

public class GUIDriver {
    public static void main(String[] args) {
        //set up a gui for inputting the ip address
        JFrame ipGetter = new JFrame();
        JPanel ipPanel = new JPanel();

        ipPanel.setLayout(new GridLayout(2,2));
        ipPanel.add(new JLabel("Enter the IP address: "));

        //text field of the ip address
        JTextField textField = new JTextField();
        ipPanel.add(textField);

        //empty label
        ipPanel.add(new JLabel());

        //add a button for the ip address
        JButton submitButton = new JButton("Submit");
        submitButton.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent arg0) {
                String ipAddress = textField.getText();
                int ipPort = 55555;
                try{
                    BufferedReader inFromKeyboard = new BufferedReader(new InputStreamReader(System.in));

                    Socket clientSocket = new Socket(ipAddress, ipPort);

                    DataOutputStream outToServer = new DataOutputStream(clientSocket.getOutputStream());
                    BufferedReader inFromServer = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));

                    System.out.println("Enter in a request to the server: ");

                    String request = inFromKeyboard.readLine();
                    outToServer.writeBytes(request + "\n");

                    String responseFromServer = inFromServer.readLine();
                    System.out.println("Server response: " + responseFromServer);

                    //close the socket after we are done with it
                    clientSocket.close();

                    
                    CodeDocument document = new CodeDocument();

                    CodeGUI cg = new CodeGUI(document);
                    CodeGUI cg2 = new CodeGUI(document);

                    ipGetter.setVisible(false);
                }
                catch(UnknownHostException e){
                    System.out.println("Could not connect to host. ");
                    e.printStackTrace();
                }
                catch(IOException e){
                    System.out.println("Exception in socket.");
                    e.printStackTrace();
                }
			}
        });
        ipPanel.add(submitButton);

        ipGetter.add(ipPanel);

        ipGetter.setSize(500, 200);
        ipGetter.setLocation(100,100);
        ipGetter.setVisible(true);
    }
}