package proxies;

import java.util.*;
import java.net.*;
import java.io.*;

import subject.*;

public class CodeDocumentProxy{

    CodeDocumentProxy() throws IOException{

    CodeDocument document = new CodeDocument();
    
    ServerSocket welcomeSocket = new ServerSocket(55555);

    while(true)
    {
        //wait for clients to enter
        Socket mySocket = welcomeSocket.accept();

        String clientIP = mySocket.getInetAddress().getHostAddress();
        System.out.println("The IP address of the client making a request is: " + clientIP);

        DataOutputStream outToClient = new DataOutputStream(mySocket.getOutputStream());
        BufferedReader inFromClient = new BufferedReader(new InputStreamReader(mySocket.getInputStream()));

        //wait to read the request from the client
        String requestFromClient = inFromClient.readLine();

        //print out the request to the screen
        System.out.println("Client Request: " + requestFromClient);

        if(requestFromClient.equalsIgnoreCase("ms"))
        {
            //the request is for the number of milliseconds
            //since Jan 1, 1970. Send back that number.

            //convert that number to a string
            String numMs = String.valueOf( (new Date()).getTime() );

            //write that string value to the socket
            outToClient.writeBytes(numMs + "\n");
        }
        else if(requestFromClient.equalsIgnoreCase("exit")){
            mySocket.close();
            break;
        }
        else if(requestFromClient.equalsIgnoreCase("gettext")){
            outToClient.writeBytes(document.getText()+"\n");
        }
        else
        {
            outToClient.writeBytes("Invalid Request!\n");
        }

        //close the socket
        mySocket.close();
    }
    welcomeSocket.close();
    }
}