package observer;

import javax.swing.*;

import subject.*;

public class CodeGUI extends JFrame implements Observer {
    CodeDocumentInterface document;
    JTextArea textArea;
    boolean settingText;

    public CodeGUI(CodeDocumentInterface document) {
        settingText = false;
        this.document = document;
        document.attach(this);

        textArea = new JTextArea();
        textArea.setText(document.getText());
        textArea.getDocument().addDocumentListener(new CodeDocumentListener(this));
        this.add(textArea);

        this.setSize(500, 500);
        this.setLocation(100, 100);
        this.setVisible(true);
    }

    @Override
    public void update() {
        if(!settingText){
            settingText = true;
            textArea.setText(document.getText());
            settingText = false;
        }
    }

    
}