package app;

public class Driver {
    public static void main(String[] args) throws Exception {
        Icon i1 = new Icon(5,5);
        System.out.println(i1.toString());
        i1.setRGB(0, 0, 255, 0, 0);
        i1.setRGB(0, 4, 0, 255, 0);
        i1.setRGB(4, 0, 0, 0, 255);
        i1.setRGB(4, 4, 0, 0, 0);
        System.out.println(i1.toString());
        //i1.setRGB(-1,-1,0,0,0);
        //System.out.println(i1.getRGB(-1,-1));
        i1.toBitMap("test.bmp");
    }
}