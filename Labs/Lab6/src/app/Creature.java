package app;

public class Creature{
    //creatures have ages and a status (1 = alive, 0 = dead)
    private int age;
    private boolean status;
    Creature(){
        age = 0;
        status = true;
    }
    Creature(int age){
        this.age = age;
        status = true;
    }

    public int getAge(){
        return age;
    }
    public void setAge(int age){
        if(age<0){
            age = 0;
        }
        this.age = age;
    }
    public boolean getStatus(){
        return status;
    }
    public void setStatus(boolean status){
        this.status = status;
    }
}