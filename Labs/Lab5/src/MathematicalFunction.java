import java.util.ArrayList;
import java.util.List;

/**
 * This can hold the terms that make up a mathematical function. One can add terms to the function
 * using addTerm(). 
 * 
 * The height at an x value can be calculated by calling getHeightAtX().
 * 
 * A string representation of the function can be created for printing.
 * 
 */
public class MathematicalFunction
{
	//holds all of the terms of a mathematical function
	private List < Term > terms;

	public MathematicalFunction()
	{
		//all functions start with no terms
		terms = new ArrayList < Term >();
	}
	
	/**
	 * Adds a term to the integral calculator.
	 * 
	 * @param c The coefficient of the new term to add
	 * @param e The exponent of the new term to add
	 */
	public void addTerm(int c, int e)
	{
		//create a new term 
		Term t = new Term(c, e);
		
		//add a reference to the term to a list
		terms.add(t);
	}
	
	/**
	 * Returns the number of terms being stored
	 * 
	 * @return The number of terms stored in this mathematical function
	 */
	public int getNumberOfTerms()
	{
		//the number of terms stored in the list
		return terms.size();
	}

	/**
	 * Returns a term at a specific index.
	 * 
	 * @param pos The position of the term (zero based)
	 * @return The term at the requested position if it exists, a null pointer otherwise
	 */
	public Term getTerm(int pos)
	{
		Term retVal = null;

		//if the requested position is in the bounds of the list
		if(pos >= 0 && pos < terms.size())
		{
			//get the requested term
			retVal = terms.get(pos);
		}

		return retVal;
	}

	/**
	 * Returns the height at a point in the function.
	 * 
	 * @param x The point on the x axis where the function should be evaluated.
	 * @return The sum of all the terms in the function
	 */
	public double getHeightAtX(double x)
	{
		//the sum of all the terms at the point x 
		double height = 0.0;
		
		//go through all of the terms
		for(Term term : terms)
		{
			//get the height of each term at x and add it to a running sume
			height += term.evaluateAtX(x);
		}
		
		return height;
	}
	/**
	 * A function that returns a readable representation of a mathematical function
	 * 
	 * @return A string with all of the terms in it. 
	 */
	@Override
	public String toString()
	{
		StringBuilder builder = new StringBuilder();
		
		//go through all of the terms
		for(int i = 0;i < terms.size();i++)
		{
			//get the string representation of the term
			String termString = terms.get(i).toString();

			//if this is not the first term, handle the +/- operators in between terms
			if(i > 0)
			{
				//if the current term has a negative coefficient
				if(terms.get(i).getCoefficient() < 0)
				{
					//add a subtraction operator before printing the term
					builder.append(" - ");

					//strip of the leading '-' from the term (since we added a subtration operator above)
					termString = termString.substring(1);
				}
				else //non negative term, add a plus operator
				{
					builder.append(" + ");
				}					
			}
			
			//add the term (perhaps without the negative sign)
			builder.append(termString);					
		}
		
		return builder.toString();
	}
	
	//quick and dirty testing of this class
	public static void main(String[] args)
	{
		//create a function
		MathematicalFunction mathFunction = new MathematicalFunction();
		
		//add a term x^2
		mathFunction.addTerm(1, 2);
		
		//get the y value at different x's
		for(double x = 1.0;x <= 2.0;x = x + .1)
		{
			System.out.printf("At x:%s the height y: %s\n", x, mathFunction.getHeightAtX(x));
		}
	}
}
