/**
 * This is an abstract class that will find the area under a curve. The method of calculating
 * the area is left for a subclass. The class does provide all the functionality other than
 * finding the area. It holds a mathematical function and the bounds. One can ask what the
 * height of the function is at any x value.
 */
public abstract class IntegralCalculator
{
	//holds a mathematical function
	protected MathematicalFunction mathFunction;
	
	//the bounds of the integral
	private double lowerBound;
	private double upperBound;
	
	/**
	 * Creates a plain, empty integral calculator
	 */
	public IntegralCalculator()
	{
		//create an empty math function
		mathFunction = new MathematicalFunction();
		
		//default bounds
		setBounds(0.0, 0.0);
	}
	
	/**
	 * Calculates the area under a curve. This must be 'filled in' by a subclass. Inherit from this
	 * class and provide an implementation for calculate().
	 * 
	 * @return The area under a curve from the lower bound to the upper bound
	 */
	public abstract double calculate();
	
	/**
	 * Get a string with the result of calculating the integral.
	 * 
	 * @return A string describing the integral (function and bounds) and the area under the curve.
	 */
	public String getResult()
	{
		StringBuilder builder = new StringBuilder();
		
		builder.append("The integral from lower bound: ");
		builder.append(getLowerBound());
		builder.append(" to upper bound: ");
		builder.append(getUpperBound());
		builder.append("\nfor the function: ");
		builder.append(mathFunction.toString());
		builder.append(" is ");
		
		//THIS LINE IS SPECIAL/COOL/NEAT!!!
		//we are calling a method that is not defined yet but we know that it will filled in later by a subclass
		//it is ok that there is no code yet, it will be called polymorphically from the derived class that extends this one
		builder.append(calculate());
		//=============***********==
		
		return builder.toString();
	}
	
	/**
	 * Adds a term to the mathematical function for this integral. 
	 * 
	 * @param c Coefficient for the new term
	 * @param e Exponent for the new term
	 */
	public void addTerm(int c, int e)
	{
		//add the term to the function
		mathFunction.addTerm(c, e);
	}

	//getters and setters
	public double getLowerBound()
	{
		return lowerBound;
	}

	public double getUpperBound()
	{
		return upperBound;
	}

	/**
	 * Set the upper and lower bounds. Verifies that the lower comes before or is equal to the upper bound. 
	 * 
	 * @param lowerBound Lower bound of the integral.
	 * @param upperBound Upper bound of the integral.
	 */
	public void setBounds(double lowerBound, double upperBound)
	{
		if(lowerBound <= upperBound)
		{
			this.lowerBound = lowerBound;
			this.upperBound = upperBound;			
		}
		//else- keep the old bounds
	}
}
