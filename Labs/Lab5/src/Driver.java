/**
 * This is a driver class to run the integral calculator.
 */
public class Driver
{
	public static void main(String[] args)
	{
		//create an integral calculator
		IntegralCalculator integralCalculator = null;

		//create an instance of a fully functional integral calculator here
		integralCalculator = new RiemannSum();
		
		//add the bounds: 1.0 - 2.0
		integralCalculator.setBounds(1.0, 2.0);
		
		//add some terms: 5x^2 + 5x + 5 
		integralCalculator.addTerm(5, 2);
		integralCalculator.addTerm(5, 1);
		integralCalculator.addTerm(5, 0);
		
		//print the result of calculating the integral (~24.167)
		System.out.println(integralCalculator.getResult());
	}
}
