/**
 * This class represents a term in a mathematical function that has a coefficient and an exponent.
 * 
 * It can be evaluated at any point, x, that the user chooses using evaluate().
 * 
 * It can create a string representation of itself for printing. 
 * 
 */
public class Term
{
	//data members for a mathematical term
	private int coefficient;
	private int exponent;
	//a stand in for the variable, almost always x in textbooks
	private static char variableRepresentation = 'x';
	
	/**
	 * A term represents an element of a mathematical function. A mathematical function can have 
	 * many terms.
	 * 
	 * @param c The coefficient of a term. If the term is 2x^3 then the coefficient is 2
	 * @param e The exponent for a term. If the term is 2x^3 then the exponent is 3
	 */
	public Term(int c, int e)
	{
		setCoefficient(c);
		setExponent(e);
	}
	
	/**
	 * Evaluates a term at a particular x value.
	 * 
	 * @param xValue The x value that will be filled in 
	 * @return The product of the coefficient and the x value raised to the term's exponent
	 */
	public double evaluateAtX(double xValue)
	{		
		return (double)getCoefficient() * Math.pow(xValue, (double)getExponent());
	}
	
	/**
	 * Returns a string representation of a term. The ^ is used as an exponentiation symbol.
	 *  
	 * @return The method accounts for a 0 coefficient (return a 0), a 1 coefficient 
	 * (leaves the coefficient out), an exponent of 1 (no exponent is shown), an exponent of 0 (no 
	 * variable or exponent is shown).
	 */
	@Override
	public String toString()
	{
		//holds the string representation of a term
		StringBuilder builder = new StringBuilder();
		
		//if there is a non-zero coefficient
		if(getCoefficient() != 0)
		{
			//start with the coefficient 
			
			//coefficients of +/-1 do not need any representation, however a -1 should keep the sign
			if(getCoefficient() == -1)
			{
				//if this term has a -1 coefficient and a 0 exponent
				if(getExponent() == 0)
				{
					//print out the -1
					builder.append(getCoefficient());
				}
				else //term has a -1 coefficient and a non-0 exponent 
				{
					builder.append("-");					
				}
			}			
			else if(getCoefficient() != 1) //not -1 or +1
			{
				builder.append(getCoefficient());				
			}			
			
			//now handle the exponents
			//exponents of one just show the variable representation
			if(getExponent() == 1)
			{
				//show the variable representation
				builder.append(getVariableRepresentation());
			}
			else if(getExponent() != 0) //not 1 and not 0, show
			{
				//show the variable and an exponent character ^
				builder.append(getVariableRepresentation());
				builder.append("^");
				//show the exponent
				builder.append(getExponent());
			}
		}
		else //0 coefficient means this term does not contribute anything
		{
			//put a 0 to represent no output
			builder.append("0");
		}
		
		return builder.toString();
	}
	
	//public getters and setters
	public int getCoefficient()
	{
		return coefficient;
	}
	public void setCoefficient(int coefficient)
	{
		this.coefficient = coefficient;
	}
	public int getExponent()
	{
		return exponent;
	}
	public void setExponent(int exponent)
	{
		this.exponent = exponent;
	}
	public static char getVariableRepresentation()
	{
		return variableRepresentation;
	}
	public static void setVariableRepresentation(char variableRepresentation)
	{
		Term.variableRepresentation = variableRepresentation;
	}
	
	//for quick Term testing
	public static void main(String[] args)
	{
		//2x^2
		Term t1 = new Term(2, 2);
		System.out.printf("Term: %s\n", t1.toString());
		System.out.printf("Eval: %s\n", t1.evaluateAtX(1.0));
		
		//3x
		Term t2 = new Term(3, 1);
		System.out.printf("Term: %s\n", t2.toString());
		System.out.printf("Eval: %s\n", t2.evaluateAtX(1.0));

		//4
		Term t3 = new Term(4, 0);
		System.out.printf("Term: %s\n", t3.toString());
		System.out.printf("Eval: %s\n", t3.evaluateAtX(1.0));

		//2x
		Term t4 = new Term(2, 1);
		System.out.printf("Term: %s\n", t4.toString());
		System.out.printf("Eval: %s\n", t4.evaluateAtX(1.0));

		//0
		Term t5 = new Term(0, 1);
		System.out.printf("Term: %s\n", t5.toString());
		System.out.printf("Eval: %s\n", t5.evaluateAtX(1.0));

		//-3x^-1
		Term t6 = new Term(-3, -1);
		System.out.printf("Term: %s\n", t6.toString());
		System.out.printf("Eval: %s\n", t6.evaluateAtX(1.0));

		//x
		Term t7 = new Term(1, 1);
		System.out.printf("Term: %s\n", t7.toString());
		System.out.printf("Eval: %s\n", t7.evaluateAtX(1.0));	
		
		//-x
		Term t8 = new Term(-1, 1);
		System.out.printf("Term: %s\n", t8.toString());
		System.out.printf("Eval: %s\n", t8.evaluateAtX(1.0));
		
		//-4
		Term t9 = new Term(-4, 0);
		System.out.printf("Term: %s\n", t9.toString());
		System.out.printf("Eval: %s\n", t9.evaluateAtX(1.0));
	}
}
