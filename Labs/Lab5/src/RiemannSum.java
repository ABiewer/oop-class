public class RiemannSum extends IntegralCalculator{
    /**
     * Calculates the left riemann sum as an Integral Calculator
     */
    public double calculate(){
        //running counter
        double sum = 0;

        //arbitrarily defined as 2000 steps
        int numSteps = 2000;

        //the lower bound is required for each of the calculations, so it is saved
        double lb = getLowerBound();

        //the width over the bounds
        double width = getUpperBound()-lb;

        //the step size as an x value
        double dx = width/numSteps;

        //this is where the calculation happens
        for(int i = 0;i<numSteps;i++){
            double loc = (width) * (i/ (double)numSteps )  + lb;
            sum += mathFunction.getHeightAtX(loc)*dx;
        }

        return sum;
    }
}