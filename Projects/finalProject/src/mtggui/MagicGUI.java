package mtggui;

import java.awt.*;

import javax.swing.*;

import deck.*;
import mtg.Proxy;

public class MagicGUI extends JFrame{
    private Proxy myProxy;

    private boolean isMyTurn;
    public boolean playedLand;

    public JPanel bottomPanel;
    public JPanel fullView;

    public JLabel theirLife;
    public JLabel myLife;

    public JLabel myCreatures;
    public JLabel myLands;
    public JLabel theirCreatures;
    public JLabel theirLands;

    public AttackButton attackButton;

    private JLabel turnIndicator;

    public MagicGUI(Hand hand, boolean isMyTurn, Proxy myProxy){
        super("Magic The Gathering");

        this.myProxy = myProxy;

        this.isMyTurn = isMyTurn;

        fullView = new JPanel(new BorderLayout());

        JPanel topPanel = new JPanel(new GridLayout(2,2));

        topPanel.add(new JLabel("Your Life"));
        topPanel.add(new JLabel("Their Life"));
        myLife = new JLabel("20");
        topPanel.add(myLife);
        theirLife = new JLabel("20");
        topPanel.add(theirLife);

        fullView.add(topPanel, BorderLayout.PAGE_START);

        bottomPanel = new JPanel(new GridLayout(1,7));

        String handString = hand.toString();

        for(int i = 0;i<7;i++){
            bottomPanel.add(new CardButton(i,handString.charAt(i) - '0',this));
        }

        fullView.add(bottomPanel, BorderLayout.PAGE_END);


        attackButton = new AttackButton(this);
        fullView.add(attackButton, BorderLayout.LINE_END);

        if(isMyTurn){
            turnIndicator = new JLabel("Your Turn");
        }
        else{
            turnIndicator = new JLabel("Their Turn");
        }
        fullView.add(turnIndicator, BorderLayout.LINE_START);


        JPanel middlePanel = new JPanel(new GridLayout(4,2));
        middlePanel.add(new JLabel("Lands:"));
        theirLands = new JLabel("0");
        middlePanel.add(theirLands);
        middlePanel.add(new JLabel("Creatures:"));
        theirCreatures = new JLabel("0");
        middlePanel.add(theirCreatures);
        middlePanel.add(new JLabel("Creatures:"));
        myCreatures = new JLabel("0");
        middlePanel.add(myCreatures);
        middlePanel.add(new JLabel("Lands:"));
        myLands = new JLabel("0");
        middlePanel.add(myLands);
        fullView.add(middlePanel, BorderLayout.CENTER);

        this.add(fullView);

        setLocation(100,100);
        setSize(800,600);
        setVisible(true);

        if(isMyTurn){
            playATurn();
        }
    }

    public boolean isMyTurn(){
        return isMyTurn;
    }

    public void playATurn(){
        upkeep();
        //turn continued through buttons
    }

    public void upkeep(){
        playedLand = false;
        myProxy.hand.pullOne();
        updateBottomPanel();
    }

    public void playCard(int index){

    }

    public void attack() throws Exception {
        myProxy.attack();
        if(myProxy.hand.size()>7){
            myProxy.hand.playCard(0);
            updateBottomPanel();
        }
        changeTurn();
    }

    public void addLand(int index){
        playedLand = true;
        myProxy.user.lands++;
        myLands.setText(Integer.toString(myProxy.user.lands));
        myProxy.hand.playCard(index);

        //bottomPanel.remove(index);
        updateBottomPanel();

        myProxy.incrementLands();
    }

    public void updateOpponentLife(int opponentLife){
        theirLife.setText(Integer.toString(opponentLife));
    }

    public void changeTurn(){
        isMyTurn = !isMyTurn;
        if(isMyTurn){
            turnIndicator.setText("Your Turn");
            playATurn();
        }
        else{
            turnIndicator.setText("Their Turn");
        }
    }

    private void updateBottomPanel(){
        //update the hand indicator panel
        bottomPanel.removeAll();
        bottomPanel = new JPanel(new GridLayout(1,myProxy.hand.size()));
        String handString = myProxy.hand.toString();
        for(int i = 0;i<myProxy.hand.size();i++){
            bottomPanel.add(new CardButton(i,handString.charAt(i) - '0',this));
        }
        fullView.add(bottomPanel, BorderLayout.PAGE_END);
        fullView.revalidate();
        //fullView.repaint();
        this.validate();
        //this.repaint();
    }

    public void playACreature(int index, int value){
        myProxy.user.creatureStrength+=value;
        myProxy.hand.playCard(index);
        myCreatures.setText(Integer.toString(myProxy.user.creatureStrength));
        updateBottomPanel();
        myProxy.addCreature();
    }

    public int numLands(){
        return myProxy.user.lands;
    }
}