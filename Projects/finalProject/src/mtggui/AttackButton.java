package mtggui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;

public class AttackButton extends JButton {

    private MagicGUI myGUI;

    public AttackButton(MagicGUI myGUI) {
        this.myGUI = myGUI;

        this.setText("ATTACK!");

        addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent arg0) {
                if (myGUI.isMyTurn()) {
                    try {
                        myGUI.attack();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
			}
        });
    }
}