package mtggui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;

public class CardButton extends JButton {
    private int index;
    private int value;
    private MagicGUI myGUI;

    public CardButton(int index, int value, MagicGUI myGUI) {
        this.index = index;
        this.value = value;
        this.myGUI = myGUI;
        if(value == 0){
            this.setText("Land");
        }
        else{
            this.setText("Creature: " + Integer.toString(value));
        }
        
        addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent arg0) {
                if(myGUI.isMyTurn()){
                    //land
                    if(value == 0){
                        if(!myGUI.playedLand){
                            //remove card
                            myGUI.addLand(index);
                        }
                        //do nothing
                    }
                    //creature
                    else{
                        if(myGUI.numLands()>=value){
                            myGUI.playACreature(index, value);
                        }
                    }
                }
            }
            
        });
    }
}