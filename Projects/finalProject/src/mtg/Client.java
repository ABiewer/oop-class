package mtg;

import java.net.*;
import java.io.*;


public class Client{
    private Proxy getter;
    public int lands;
    public int creatureStrength;
    public int life;

    public Client(int thisPort) throws Exception{
        getter = new Proxy(this, thisPort);
    }

    public int defend(int attackingStrength){
        int s = creatureStrength;
        creatureStrength -= attackingStrength;

        if(creatureStrength<0){
            life += creatureStrength;
            creatureStrength = 0;
        }
        attackingStrength -= s;
        if(attackingStrength<0){
            attackingStrength = 0;
        }

        getter.updateBoardDefend(attackingStrength);

        return life;
    }

    public void attack(int theirStrength){
        int myStrength = creatureStrength;
        creatureStrength -= theirStrength;

        if(creatureStrength<0){
            creatureStrength = 0;
        }
        theirStrength -= myStrength;
        if(theirStrength<0){
            theirStrength = 0;
        }

        getter.updateBoardAttack(theirStrength);
    }
}