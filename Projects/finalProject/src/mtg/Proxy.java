package mtg;

import java.io.*;
import java.net.*;
import deck.*;
import mtggui.*;

public class Proxy {
    public Client user;
    private ClientProxy server;
    private boolean sent;

    private int theirPort;
    private String theirIP;

    private BufferedReader inFromKeyboard;
    private Socket clientSocket;
    private DataOutputStream outToServer;
    private BufferedReader inFromServer;

    public Hand hand;
    private MagicGUI gui;

    /**
     * Generate the proxy to act as the 'sender' end of this side of the program It
     * establishes a client proxy to receive requests from the other machine It then
     * tries to connect if the other person has not connected already Otherwise, it
     * sends a response to the other person that it is connected
     * 
     * @param user
     * @param thisPort
     * @throws Exception
     */
    public Proxy(Client user, int thisPort) throws Exception {
        sent = false;
        this.user = user;

        server = new ClientProxy(this, thisPort);
        System.out.println("Returned to Proxy");

        initializePipeline();

        System.out.println("initialized server, can I send a request: " + !sent);

        if (!sent) {
            sendRequest();
            System.out.println("Waiting for other player to connect...");
        } else {
            System.out.println("Sending ready message to opponent...");
            sendReadyMessage();
            System.out.println("Sent message!");
            System.out.println("Proxy is done listening.");
        }
    }

    /**
     * Starts up the whole process of connecting the first time to the opponent
     */
    private void initializePipeline() throws Exception {
        System.out.println("Welcome to Magic the Gathering!");

        printIPInfo();
        inFromKeyboard = new BufferedReader(new InputStreamReader(System.in));

        System.out.println("Enter the IP address of the machine to connect to: ");
        theirIP = inFromKeyboard.readLine();

        System.out.println("Enter the port number of the application to connect to: ");
        String PortToConnectTo = inFromKeyboard.readLine();
        theirPort = Integer.parseInt(PortToConnectTo);

        clientSocket = new Socket(theirIP, theirPort);

        outToServer = new DataOutputStream(clientSocket.getOutputStream());
        inFromServer = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
    }

    /**
     * Apparently it's VERY important that the socket gets closed after every
     * interaction, otherwise the program is just going to be unhappy and crash
     * 
     * @throws Exception
     */
    private void restartPipeline() throws Exception {
        clientSocket = new Socket(theirIP, theirPort);
        outToServer = new DataOutputStream(clientSocket.getOutputStream());
        inFromServer = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
    }

    private void printIPInfo() throws Exception {
        String myIP = InetAddress.getLocalHost().getHostAddress();
        System.out.println("My IP address is " + myIP);

        String myHostName = InetAddress.getLocalHost().getHostName();
        System.out.println("My host name is " + myHostName + "\n");
    }

    /**
     * Establishes seniority between the two programs, so that only one of them
     * generates the commands to create the board
     * 
     * @throws Exception
     */
    private void sendRequest() throws Exception {
        String request = "connect:";
        outToServer.writeBytes(request + "\n");

        if (inFromServer.readLine().startsWith("1")) {
            sent = true;
            System.out.println("Request Sent, I am first.");
            clientSocket.close();
        } else {
            sent = true;
            System.out.println("I am second!");
            clientSocket.close();
        }
    }

    /**
     * When the 2nd person to connect is done connecting, it sends a message letting
     * the other guy know that it's ready
     */
    private void sendReadyMessage() throws Exception {
        outToServer.writeBytes("ready:\n");
        System.out.println("Response: " + inFromServer.readLine());
    }

    /**
     * Begin the process of setting up the game.
     * 
     * @throws Exception
     */
    public void startGame() throws Exception {
        System.out.println("Starting the game...");

        generateHand();
        tellOpponent("generate hand:");

        generateGUI(true);
        tellOpponent("generateGUI");

        // do more stuff here after done debugging

    }

    public void tellOpponent(String command) throws Exception {
        restartPipeline();
        outToServer.writeBytes(command + "\n");
        System.out.println("response: " + inFromServer.readLine());
        clientSocket.close();
    }

    public void generateHand() {
        hand = new Hand();
        System.out.println("Hand: " + hand);
    }

    public void dontSendRequest() {
        sent = true;
    }

    public void generateGUI(boolean isMyTurn) {
        user.life = 20;
        user.creatureStrength = 0;
        user.lands = 0;
        gui = new MagicGUI(hand, isMyTurn, this);
    }

    public void attack() throws Exception {
        restartPipeline();
        outToServer.writeBytes("attack:" + user.creatureStrength + "\n");
        int opponentLife = Integer.parseInt(inFromServer.readLine());
        gui.updateOpponentLife(opponentLife);
        clientSocket.close();
        user.attack(Integer.parseInt(gui.theirCreatures.getText()));
    }

    public void changeTurn() {
        gui.changeTurn();
    }

    public void incrementLands() {
        try {
            tellOpponent("IncrementLands:");
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    public void addCreature(){
        try{
            tellOpponent("Creatures:"+Integer.toString(user.creatureStrength));
        }
        catch(Exception e){
            e.printStackTrace();
        }
    }

    public void tellGUIToIncrementLands(){
        gui.theirLands.setText( Integer.toString( Integer.parseInt( gui.theirLands.getText() ) + 1 ) );
    }

    public void tellGUIToUpdateCreatures(int value){
        gui.theirCreatures.setText( Integer.toString(value));
    }

    public void updateBoard(){
        updateMyLife(user.life);
        updateMyCreatures(user.creatureStrength);
    }

    public void updateBoardDefend(int attackingStrength){
        updateBoard();
        gui.theirCreatures.setText(Integer.toString(attackingStrength));
    }

    public void updateBoardAttack(int defendingStrength){
        updateBoard();
        gui.theirCreatures.setText(Integer.toString(defendingStrength));
    }

    public void updateMyLife(int life){
        gui.myLife.setText(Integer.toString(life));
    }

    public void updateMyCreatures(int s){
        gui.myCreatures.setText(Integer.toString(s));
    }
}