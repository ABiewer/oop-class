package mtg;

import java.net.*;
import java.io.*;

public class ClientProxy{
    private Proxy getter;
    private Thread myThread;

    private ServerSocket welcomeSocket;

    public ClientProxy(Proxy getter, int thisPort) throws Exception{
        System.out.println("Created ClientProxy");
        this.getter = getter;

        initializePipeline(thisPort);
        
        myThread = new Thread(new Runnable(){

            @Override
            public void run() {
                try{
                    while(true)
                    {
                        System.out.println("Waiting...");
                        Socket mySocket = welcomeSocket.accept();
            
                        String clientIP = mySocket.getInetAddress().getHostAddress();
                        System.out.println("The IP address of the client making a request is: " + clientIP);
            
                        DataOutputStream outToClient = new DataOutputStream(mySocket.getOutputStream());
                        BufferedReader inFromClient = new BufferedReader(new InputStreamReader(mySocket.getInputStream()));

                        String requestFromClient = inFromClient.readLine();

                        System.out.println("Client Request: " + requestFromClient);

                        if(requestFromClient.startsWith("connect:"))
                        {
                            getter.dontSendRequest();
                            outToClient.writeBytes("1 Connect\n");
                        }
                        else if(requestFromClient.startsWith("ready:")){
                            outToClient.writeBytes("Message received.\n");
                            getter.startGame();
                        }
                        else if(requestFromClient.startsWith("generate hand:")){
                            getter.generateHand();
                            outToClient.writeBytes("Message received.\n");
                        }
                        else if(requestFromClient.startsWith("generateGUI")){
                            getter.generateGUI(false);
                            outToClient.writeBytes("Message received.\n");
                        }
                        else if(requestFromClient.startsWith("attack:")){
                            int creatureStrength = Integer.parseInt(requestFromClient.substring(7));
                            String responseLife = Integer.toString(getter.user.defend(creatureStrength));
                            outToClient.writeBytes(responseLife + "\n");
                            getter.changeTurn();
                        }
                        else if(requestFromClient.startsWith("IncrementLands:")){
                            getter.tellGUIToIncrementLands();
                            outToClient.writeBytes("Message received\n");
                        }
                        else if(requestFromClient.startsWith("Creatures:")){
                            getter.tellGUIToUpdateCreatures(Integer.parseInt(requestFromClient.substring(10)));
                            outToClient.writeBytes("Message received\n");
                        }
                        else
                        {
                            outToClient.writeBytes("Invalid Request!\n");
                        }

                        System.out.println("Finished request: " + requestFromClient);

                        mySocket.close();
                        inFromClient.close();
                        outToClient.close();
                    }
                }
                catch(IOException e){
                    System.out.println("Encountered an error in the thread:");
                    e.printStackTrace();
                    System.out.println("\nPlease try restarting the program.");
                }
                catch(Exception e){
                    System.out.println("Encountered an exception:");
                    e.printStackTrace();
                }
            }
            
        });

        startListening();

        System.out.println("Created Thread.");
    }

    private void initializePipeline(int portNum) throws Exception{
        welcomeSocket = new ServerSocket(portNum);
    }

    private void startListening(){
        myThread.start();
    }
}