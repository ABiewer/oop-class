package deck;

import java.util.ArrayList;
import java.util.Random;

public class Deck {
    private ArrayList<Card> deck;
    private Random random;

    public Deck(){
        deck = new ArrayList<Card>();
        for(int i = 0;i<30;i++){
            deck.add(new Card("land"));
        }
        for(int i = 0;i<15;i++){
            deck.add(new Card(1));
        }
        for(int i = 0;i<10;i++){
            deck.add(new Card(2));
        }
        for(int i = 0;i<5;i++){
            deck.add(new Card(3));
        }
        random = new Random();
    }

    public Card pullCard(){
        int index = random.nextInt(deck.size());
        Card retval = deck.get(index);
        deck.remove(index);
        return retval;
    }
}