package deck;



public class Card{
    public int strength;

    public Card(String land){
        strength = 0; //Land Card
    }
    public Card(int i){
        strength = i;
    }

    public String toString(){
        String retval = Integer.toString(strength);
        return retval;
    }
}