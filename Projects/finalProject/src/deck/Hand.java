package deck;

import java.util.ArrayList;

public class Hand{
    private Deck deck;
    private ArrayList<Card> hand;

    public Hand(){
        deck = new Deck();
        hand = new ArrayList<Card>();
        pull7();
    }

    private void pull7(){
        for(int i = 0;i<7;i++){
            hand.add(deck.pullCard());
        }
    }

    public String toString(){
        String retval = "";
        for(Card c : hand){
            retval += c.toString();
        }
        return retval;
    }

    public void removeFirst(){
        hand.remove(0);
    }

    public void pullOne(){
        hand.add(deck.pullCard());
    }

    public void playCard(int index){
        hand.remove(index);
    }

    public int size(){
        return hand.size();
    }
}